#define FLAG_KEY 20
#define FLAG_BEAN 21

@none
@any

@status
at intro0
anykey
goto intro2
desc

at intro1
anykey
goto cottage
desc

at intro2
anykey
goto intro1
desc

@n
'*'
at gallery
absent open_door
"The door is locked."
done

'*'
at gallery
present open_door
goto huge_room
desc

@w
'*'
at huge_room
absent broken_door
"The door is locked."
done

'*'
at huge_room
present broken_door
goto green_room
desc

@x
'bean'
present bean
"It's a magic bean, perhaps you should\nplant it."
done

'bean'
present beanstalk
"Perhaps you could climb it!"
done

'bench'
at kitchen
zero FLAG_KEY
"On the bench you find a silver key."
create key
set FLAG_KEY
done

'bench'
at kitchen
notzero FLAG_KEY
"You see nothing special."
done

'key'
present key
"It's just a silver key."
done

'mouse'
present mouse
"It's just a tiny mouse."
done

'egg'
present egg
"It looks valuable - it could set you up\nfor life!"
done

'garden'
at garden
absent beanstalk
"The garden looks ready for planting."
done

'garden'
at garden
present beanstalk
"The garden has a giant beanstalk growing\nin it."
done

'shed'
at garden
"You see nothing special about the shed."
done

'mother'
present mother_1
"Mother is unhappy because Peter and\nMary have been captured by the mean\nGiant."
done

'mother'
present mother_2
"Mother is unhappy because Peter and\nMary have been captured by the mean\nGiant."
done

'mother'
present mother_3
"Mother is happy now that the children\nare back home."
done

'children'
present children
notat cottage
"They look frightened."
done

'axe'
present axe
"Looks sharp!"
done

'giant'
present giant
"He looks mean!"
done

'gallery'
at gallery
"You see nothing special about the\ngallery."
done

'kitchen'
at kitchen
"You see nothing special about the\nkitchen."
done

'castle'
atlt top_of_beanstalk
"This is where the mean giant lives!"
done

'castle'
"This is the castle where the giant lives."
done

'cottage'
at cottage
"You see nothing special about the\ncottage."
done

'*'
"You see nothing special."
done

@plant
'bean'
at garden
carried bean
destroy bean
create beanstalk
set FLAG_BEAN
"You plant the bean which grows very\nfast into a giant beanstalk."
done

'bean'
notat garden
zero FLAG_BEAN
"You can't do that here!"
done

'*'
"You can't do that."
done

@open
'door'
at gallery
absent open_door
carried key
"OK. You unlock the door with the silver\nkey."
create open_door
done

'door'
at huge_room
absent broken_door
carried key
"The key doesn't fit this lock."
done

'door'
at gallery
present open_door
"It's already open."
done

'door'
at huge_room
notcarr key
"The door is locked."
done

'*'
"You can't do that."
done

@break
'door'
at huge_room
absent broken_door
carried axe
"You break down the door with the axe."
create broken_door
done

'door'
at huge_room
absent broken_door
notcarr axe
"How?"
done

'door'
at huge_room
present broken_door
"It's already broken."
done

'bean'
present beanstalk
"That wouldn't be wise."
done

'*'
"You can't do that."
done

@u
'bean'
at garden
present beanstalk
goto top_of_beanstalk
desc

@go
'door'
at gallery
absent open_door
"The door is locked."
done

'door'
at gallery
present open_door
goto huge_room
desc

'door'
at huge_room
present broken_door
goto green_room
desc

@help
'*'
"Sorry!"
done

@give
'egg'
at cottage
present mother_1
carried egg
swap mother_1 mother_2
destroy egg
"Mother is pleased with the GOLDEN EGG,\nnow if only you could find THE CHILDREN."
done

'egg'
at cottage
present mother_3
carried egg
cls
"Mother is so pleased that you have been\nable to find the GOLDEN EGG which will\nset you up for life and with THE\nCHILDREN home safe and sound you all\nlive happily ever after."
"\n     C O N G R A T U L A T I O N S\n\n           This game is over\n"
end

'children'
at cottage
present mother_1
carried children
swap mother_1 mother_3
destroy children
"Mother is so pleased to see THE CHILDREN\nagain, now if only you could find the\nGOLDEN EGG."
done

'children'
at cottage
present mother_2
carried children
cls
"Mother is so pleased to see THE CHILDREN\nagain and with the GOLDEN EGG you are\nset up for life and live happily ever\nafter."
"\n     C O N G R A T U L A T I O N S\n\n           This game is over\n"
end

@hit
'giant'
present giant
"You must be joking!!! You wouldn't\nstand a chance!"
done

@wait
'*'
"What for?"
done

@turn
'*'
"You don't need to turn anything to\ncomplete this adventure."
done

@twist
'*'
"You don't need to twist anything to\ncomplete this adventure."
done

@pull
'*'
"You don't need to pull anything to\ncomplete this adventure."
done

@push
'*'
"You don't need to push anything to\ncomplete this adventure."
done

@get

'egg'
notat huge_room
get egg
ok

'egg'
at huge_room
present giant
"The giant won't let you."
done

'egg'
at huge_room
absent giant
get egg
ok

'i'
inven

'any'
autog
ok

@drop

'mouse'
notat huge_room
drop mouse
ok

'mouse'
at huge_room
present giant
"The mouse frightens the giant who runs\naway as fast as he can."
destroy giant
destroy mouse
done

'any'
autod
ok

@i
'*'
inven

@r
'*'
desc

@q
'*'
quit
turns
end

@save
'*'
save

@load
'*'
load

@verify
'*'
"Version 1.2"
done
