#define CAPACITY 3

/*
   NOTCREATED
   WORN
   CARRIED
*/

Items Item[] = { /* 0-254 */
/* mouse */
   {
      L"* A tiny mouse",
      dining_room
   },
/* key */
   {
      L"* A silver key",
      NOTCREATED
   },
/* axe */
   {
      L"* An axe",
      garden_shed
   },
/* mother_1 */
   {
      L"* Mother is here",
      cottage
   },
/* children */
   {
      L"* The Children, Peter and Mary, are here.",
      green_room
   },
/* giant */
   {
      L"* The --- G I A N T ---",
      huge_room
   },
/* egg */
   {
      L"* The GOLDEN EGG",
      huge_room
   },
/* bean */
   {
      L"* A bean",
      cottage
   },
/* beanstalk */
   {
      L"* A giant beanstalk",
      NOTCREATED
   },
/* open_door */
   {
      L"* An open door",
      NOTCREATED
   },
/* broken_door */
   {
      L"* A broken door",
      NOTCREATED
   },
/* mother_2 */
   {
      L"* Mother is here holding the GOLDEN EGG.",
      NOTCREATED
   },
/* mother_3 */
   {
      L"* Mother is here width THE CHILDREN.",
      NOTCREATED
   }
};
