const wchar_t *message[] = { /* 0-254 */
/* 0 */
	L"The door is locked.",
/* 1 */
	L"It's a magic bean, perhaps you should\nplant it.",
/* 2 */
	L"Perhaps you could climb it!",
/* 3 */
	L"On the bench you find a silver key.",
/* 4 */
	L"You see nothing special.",
/* 5 */
	L"It's just a silver key.",
/* 6 */
	L"It's just a tiny mouse.",
/* 7 */
	L"It looks valuable - it could set you up\nfor life!",
/* 8 */
	L"The garden looks ready for planting.",
/* 9 */
	L"The garden has a giant beanstalk growing\nin it.",
/* 10 */
	L"You see nothing special about the shed.",
/* 11 */
	L"Mother is unhappy because Peter and\nMary have been captured by the mean\nGiant.",
/* 12 */
	L"Mother is happy now that the children\nare back home.",
/* 13 */
	L"They look frightened.",
/* 14 */
	L"Looks sharp!",
/* 15 */
	L"He looks mean!",
/* 16 */
	L"You see nothing special about the\ngallery.",
/* 17 */
	L"You see nothing special about the\nkitchen.",
/* 18 */
	L"This is where the mean giant lives!",
/* 19 */
	L"This is the castle where the giant lives.",
/* 20 */
	L"You see nothing special about the\ncottage.",
/* 21 */
	L"You plant the bean which grows very\nfast into a giant beanstalk.",
/* 22 */
	L"You can't do that here!",
/* 23 */
	L"You can't do that.",
/* 24 */
	L"OK. You unlock the door with the silver\nkey.",
/* 25 */
	L"The key doesn't fit this lock.",
/* 26 */
	L"It's already open.",
/* 27 */
	L"You break down the door with the axe.",
/* 28 */
	L"How?",
/* 29 */
	L"It's already broken.",
/* 30 */
	L"That wouldn't be wise.",
/* 31 */
	L"Sorry!",
/* 32 */
	L"Mother is pleased with the GOLDEN EGG,\nnow if only you could find THE CHILDREN.",
/* 33 */
	L"Mother is so pleased that you have been\nable to find the GOLDEN EGG which will\nset you up for life and with THE\nCHILDREN home safe and sound you all\nlive happily ever after.",
/* 34 */
	L"\n     C O N G R A T U L A T I O N S\n\n           This game is over\n",
/* 35 */
	L"Mother is so pleased to see THE CHILDREN\nagain, now if only you could find the\nGOLDEN EGG.",
/* 36 */
	L"Mother is so pleased to see THE CHILDREN\nagain and with the GOLDEN EGG you are\nset up for life and live happily ever\nafter.",
/* 37 */
	L"You must be joking!!! You wouldn't\nstand a chance!",
/* 38 */
	L"What for?",
/* 39 */
	L"You don't need to turn anything to\ncomplete this adventure.",
/* 40 */
	L"You don't need to twist anything to\ncomplete this adventure.",
/* 41 */
	L"You don't need to pull anything to\ncomplete this adventure.",
/* 42 */
	L"You don't need to push anything to\ncomplete this adventure.",
/* 43 */
	L"The giant won't let you.",
/* 44 */
	L"The mouse frightens the giant who runs\naway as fast as he can.",
/* 45 */
	L"Version 1.2",
};
