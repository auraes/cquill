#define FLAG_KEY 20
#define FLAG_BEAN 21

const byte eNone[] = {

	0
};

const byte eAny[] = {

	0
};

const byte eStatus[] = {

	7,
	At, intro0,
	Anykey,
	Goto, intro2,
	Desc,

	7,
	At, intro1,
	Anykey,
	Goto, cottage,
	Desc,

	7,
	At, intro2,
	Anykey,
	Goto, intro1,
	Desc,

	0
};

const byte eN[] = {

	9,
	ANY,
	At, gallery,
	Absent, open_door,
	Message, 0,
	Done,

	9,
	ANY,
	At, gallery,
	Present, open_door,
	Goto, huge_room,
	Desc,

	0
};

const byte eW[] = {

	9,
	ANY,
	At, huge_room,
	Absent, broken_door,
	Message, 0,
	Done,

	9,
	ANY,
	At, huge_room,
	Present, broken_door,
	Goto, green_room,
	Desc,

	0
};

const byte eX[] = {

	7,
	BEAN,
	Present, bean,
	Message, 1,
	Done,

	7,
	BEAN,
	Present, beanstalk,
	Message, 2,
	Done,

	13,
	BENCH,
	At, kitchen,
	Zero, FLAG_KEY,
	Message, 3,
	Create, key,
	Set, FLAG_KEY,
	Done,

	9,
	BENCH,
	At, kitchen,
	Notzero, FLAG_KEY,
	Message, 4,
	Done,

	7,
	KEY,
	Present, key,
	Message, 5,
	Done,

	7,
	MOUSE,
	Present, mouse,
	Message, 6,
	Done,

	7,
	EGG,
	Present, egg,
	Message, 7,
	Done,

	9,
	GARDEN,
	At, garden,
	Absent, beanstalk,
	Message, 8,
	Done,

	9,
	GARDEN,
	At, garden,
	Present, beanstalk,
	Message, 9,
	Done,

	7,
	SHED,
	At, garden,
	Message, 10,
	Done,

	7,
	MOTHER,
	Present, mother_1,
	Message, 11,
	Done,

	7,
	MOTHER,
	Present, mother_2,
	Message, 11,
	Done,

	7,
	MOTHER,
	Present, mother_3,
	Message, 12,
	Done,

	9,
	CHILDREN,
	Present, children,
	Notat, cottage,
	Message, 13,
	Done,

	7,
	AXE,
	Present, axe,
	Message, 14,
	Done,

	7,
	GIANT,
	Present, giant,
	Message, 15,
	Done,

	7,
	GALLERY,
	At, gallery,
	Message, 16,
	Done,

	7,
	KITCHEN,
	At, kitchen,
	Message, 17,
	Done,

	7,
	CASTLE,
	Atlt, top_of_beanstalk,
	Message, 18,
	Done,

	5,
	CASTLE,
	Message, 19,
	Done,

	7,
	COTTAGE,
	At, cottage,
	Message, 20,
	Done,

	5,
	ANY,
	Message, 4,
	Done,

	0
};

const byte ePlant[] = {

	15,
	BEAN,
	At, garden,
	Carried, bean,
	Destroy, bean,
	Create, beanstalk,
	Set, FLAG_BEAN,
	Message, 21,
	Done,

	9,
	BEAN,
	Notat, garden,
	Zero, FLAG_BEAN,
	Message, 22,
	Done,

	5,
	ANY,
	Message, 23,
	Done,

	0
};

const byte eOpen[] = {

	13,
	DOOR,
	At, gallery,
	Absent, open_door,
	Carried, key,
	Message, 24,
	Create, open_door,
	Done,

	11,
	DOOR,
	At, huge_room,
	Absent, broken_door,
	Carried, key,
	Message, 25,
	Done,

	9,
	DOOR,
	At, gallery,
	Present, open_door,
	Message, 26,
	Done,

	9,
	DOOR,
	At, huge_room,
	Notcarr, key,
	Message, 0,
	Done,

	5,
	ANY,
	Message, 23,
	Done,

	0
};

const byte eBreak[] = {

	13,
	DOOR,
	At, huge_room,
	Absent, broken_door,
	Carried, axe,
	Message, 27,
	Create, broken_door,
	Done,

	11,
	DOOR,
	At, huge_room,
	Absent, broken_door,
	Notcarr, axe,
	Message, 28,
	Done,

	9,
	DOOR,
	At, huge_room,
	Present, broken_door,
	Message, 29,
	Done,

	7,
	BEAN,
	Present, beanstalk,
	Message, 30,
	Done,

	5,
	ANY,
	Message, 23,
	Done,

	0
};

const byte eU[] = {

	9,
	BEAN,
	At, garden,
	Present, beanstalk,
	Goto, top_of_beanstalk,
	Desc,

	0
};

const byte eGo[] = {

	9,
	DOOR,
	At, gallery,
	Absent, open_door,
	Message, 0,
	Done,

	9,
	DOOR,
	At, gallery,
	Present, open_door,
	Goto, huge_room,
	Desc,

	9,
	DOOR,
	At, huge_room,
	Present, broken_door,
	Goto, green_room,
	Desc,

	0
};

const byte eHelp[] = {

	5,
	ANY,
	Message, 31,
	Done,

	0
};

const byte eGive[] = {

	16,
	EGG,
	At, cottage,
	Present, mother_1,
	Carried, egg,
	Swap, mother_1, mother_2,
	Destroy, egg,
	Message, 32,
	Done,

	14,
	EGG,
	At, cottage,
	Present, mother_3,
	Carried, egg,
	Cls,
	Message, 33,
	Message, 34,
	End,

	16,
	CHILDREN,
	At, cottage,
	Present, mother_1,
	Carried, children,
	Swap, mother_1, mother_3,
	Destroy, children,
	Message, 35,
	Done,

	14,
	CHILDREN,
	At, cottage,
	Present, mother_2,
	Carried, children,
	Cls,
	Message, 36,
	Message, 34,
	End,

	0
};

const byte eHit[] = {

	7,
	GIANT,
	Present, giant,
	Message, 37,
	Done,

	0
};

const byte eWait[] = {

	5,
	ANY,
	Message, 38,
	Done,

	0
};

const byte eTurn[] = {

	5,
	ANY,
	Message, 39,
	Done,

	0
};

const byte eTwist[] = {

	5,
	ANY,
	Message, 40,
	Done,

	0
};

const byte ePull[] = {

	5,
	ANY,
	Message, 41,
	Done,

	0
};

const byte ePush[] = {

	5,
	ANY,
	Message, 42,
	Done,

	0
};

const byte eGet[] = {

	7,
	EGG,
	Notat, huge_room,
	Get, egg,
	Ok,

	9,
	EGG,
	At, huge_room,
	Present, giant,
	Message, 43,
	Done,

	9,
	EGG,
	At, huge_room,
	Absent, giant,
	Get, egg,
	Ok,

	3,
	I,
	Inven,

	4,
	ANY,
	Autog,
	Ok,

	0
};

const byte eDrop[] = {

	7,
	MOUSE,
	Notat, huge_room,
	Drop, mouse,
	Ok,

	13,
	MOUSE,
	At, huge_room,
	Present, giant,
	Message, 44,
	Destroy, giant,
	Destroy, mouse,
	Done,

	4,
	ANY,
	Autod,
	Ok,

	0
};

const byte eI[] = {

	3,
	ANY,
	Inven,

	0
};

const byte eR[] = {

	3,
	ANY,
	Desc,

	0
};

const byte eQ[] = {

	5,
	ANY,
	Quit,
	Turns,
	End,

	0
};

const byte eSave[] = {

	3,
	ANY,
	Save,

	0
};

const byte eLoad[] = {

	3,
	ANY,
	Load,

	0
};

const byte eVerify[] = {

	0,
	ANY,
	Message, 45,
	Done,

	0
};

const byte *Event[] = {
	eNone,
	eAny,
	eStatus,
	eN,
	eW,
	eX,
	ePlant,
	eOpen,
	eBreak,
	eU,
	eGo,
	eHelp,
	eGive,
	eHit,
	eWait,
	eTurn,
	eTwist,
	ePull,
	ePush,
	eGet,
	eDrop,
	eI,
	eR,
	eQ,
	eSave,
	eLoad,
	eVerify,
};
