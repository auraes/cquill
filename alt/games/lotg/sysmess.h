const wchar_t *sysmess[] = { /* 0-34 */
/* 0 */
   L"Everything is dark. You can't see.\n",
/* 1 */
   L"\nYou can also see:-\n",
/* 2 */
   L"\nWhat next?\n",
/* 3 */
   L"\nWhat next?\n",
/* 4 */
   L"\nWhat next?\n",
/* 5 */
   L"\nWhat next?\n",
/* 6 */
   L"Sorry, I don't understand you.\n",
/* 7 */
   L"You can't go that way.\n",
/* 8 */
   L"You can't.\n",
/* 9 */
   L"You have with you:-\n",
/* 10 */
   L" (worn)",
/* 11 */
   L"Nothing at all.\n",
/* 12 */
   L"Do you really want to quit now?\n",
/* 13 */
   L""
   "\n              END OF GAME\n"
   "\nDo you want to try again?\n",
/* 14 */
   L"Bye for now.\n",
/* 15 */
   L"OK.\n",
/* 16 */
   L"         Press Enter to continue",
/* 17 */
   L"You have taken ",
/* 18 */
   L" turn",
/* 19 */
   L"s",
/* 20 */
   L".\n",
/* 21 */
   L"You have scored ",
/* 22 */
   L"%\n",
/* 23 */
   L"You're not wearing it.\n",
/* 24 */
   L"You can't carry any more.\n",
/* 25 */
   L"You already have it.\n",
/* 26 */
   L"It's not here.\n",
/* 27 */
   L"You can't carry any more.\n",
/* 28 */
   L"You don't have it.\n",
/* 29 */
   L"You're already wearing it.\n",
/* 30 */
   L"Y\n",
/* 31 */
   L"N\n",
/* 32 */
   L"Save failed.\n",
/* 33 */
   L"Type in filename.\n",
/* 34 */
   L"Restore failed.\n",
};
