enum { /* rooms */
    intro0,
    intro1,
    intro2,
    cottage,
    garden,
    garden_shed,
    top_of_beanstalk,
    castle_entrance,
    kitchen,
    dining_room,
    gallery,
    huge_room,
    green_room
};

const byte exit_cottage[] = { E, garden, 0 };
const byte exit_garden[] = { W, cottage, S, garden_shed, 0 };
const byte exit_garden_shed[] = { N, garden, 0 };
const byte exit_top_of_beanstalk[] = { D, garden, E, castle_entrance, 0 };
const byte exit_castle_entrance[] = { W, top_of_beanstalk, N, dining_room,
   S, kitchen, 0 };
const byte exit_kitchen[] = { N, castle_entrance, 0 };
const byte exit_dining_room[] = { S, castle_entrance, W, gallery, 0 };
const byte exit_gallery[] = { E, dining_room, 0 };
const byte exit_huge_room[] = { S, gallery, 0 };
const byte exit_green_room[] = { E, huge_room, 0 };

Rooms Room[] =  { /* 0-251 */
/* intro0 */
   {
   L""
   "****************************************\n"
   "*                                      *\n"
   "*                                      *\n"
   "*               L A N D                *\n"
   "*                                      *\n"
   "*                of the                *\n"
   "*                                      *\n"
   "*             G I A N T S              *\n"
   "*                                      *\n"
   "*                                      *\n"
   "****************************************\n"
   "\n"
   "\n"
   "        A CHILDREN'S ADVENTURE\n"
   "\n"
   "     Dedicated to Rachael Millard\n"
   "\n"
   "\n"
   "      Written by Dorothy Millard\n"
   "               (c) 1989",
   NULL
   },
/* intro1 */
   {
   L""
   "          LAND OF THE GIANTS\n"
   "\n"
   "To move from place to place type North,\n"
   "South, East, West or their abbreviations\n"
   "N,S,E,W. Try also Up and Down.\n"
   "\n"
   "(I)nventory gives you a list of what you\n"
   "are carrying.\n"
   "\n"
   "(R)edefine will describe your current\n"
   "location.\n"
   "\n"
   "(X) is an abbreviation for Examine.\n"
   "\n"
   "(Q)uit if you have had enough.\n"
   "\n"
   "VERBS - Examine, Plant, Open, Unlock,\n"
   "Break, Go, Get, Take, Drop, Give.",
   NULL
   },
/* intro2 */
   {
   L""
   "         **********************\n"
   "         *                    *\n"
   "         * LAND OF THE GIANTS *\n"
   "         *                    *\n"
   "         **********************\n"
   "\n"
   "\n"
   "\n"
   "The Children, Peter and Mary, have been\n"
   "captured by the mean GIANT.  You have\n"
   "been chosen to rescue them.\n"
   "\n"
   "You start in your cottage with Mother.\n"
   "Can you rescue the children and find the\n"
   "GOLDEN EGG?",
   NULL
   },
/* cottage */
   {
   L""
   "You are in a your small quaint cottage.\n"
   "You can go EAST into the garden.",
   exit_cottage
   },
/* garden */
   {
   L""
   "You are in the garden. You can go WEST\n"
   "into the house and SOUTH into the shed.",
   exit_garden
   },
/* garden_shed */
   {
   L""
   "You are in the garden shed.  You can go\n"
   "NORTH into the garden.",
   exit_garden_shed
   },
/* top_of_beanstalk */
   {
   L""
   "You are at the top of the beanstalk. You\n"
   "can see a large castle in the distance.\n"
   "You can go EAST and DOWN the beanstalk.",
   exit_top_of_beanstalk
   },
/* castle_entrance */
   {
   L""
   "You are in the Castle entrance.  You can\n"
   "go NORTH, WEST and SOUTH.",
   exit_castle_entrance
   },
/* kitchen */
   {
   L""
   "You are in the Castle Kitchen.  You can\n"
   "go NORTH.  There is a bench here.",
   exit_kitchen
   },
/* dining_room */
   {
   L""
   "You are in the Castle Dining Room.  You\n"
   "can go WEST and SOUTH.",
   exit_dining_room
   },
/* gallery */
   {
   L""
   "You are in the Castle Gallery.  You can\n"
   "go EAST and there is a door to the NORTH.",
   exit_gallery
   },
/* huge_room */
   {
   L""
   "You are in a Huge Room in the Castle.\n"
   "There are doors to the WEST and SOUTH.",
   exit_huge_room
   },
/* green_room */
   {
   L""
   "You are in a Green Room in the Castle.\n"
   "You can go EAST.",
   exit_green_room
   }
};
