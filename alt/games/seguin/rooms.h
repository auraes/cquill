enum {
    room0,
    room1,
    room2,
    room3,
    room4,
    room5,
    room6,
    room7,
    room8,
    room9,
    room10,
    room11,
    room12,
    room13,
    room14,
    room15,
    room16,
    room17,
    room18,
    room19,
    room20,
    room21,
    room22,
    room23,
    room24,
    room25,
    room26,
    room27,
    room28,
    room29,
    room30,
    room31,
    room32,
    room33,
    room34,
    room35,
    room36
};

const byte exit_room1[] =  { N, room4, 0 };
const byte exit_room2[] =  { E, room3, 0 };
const byte exit_room3[] =  { N, room6, E, room4, W, room2, 0 };
const byte exit_room4[] =  { N, room7, S, room1, 0 };
const byte exit_room5[] =  { N, room8, 0 };
const byte exit_room7[] =  { S, room4, W, room6, 0 };
const byte exit_room9[] =  { S, room6, 0 };
const byte exit_room11[] = { D, room2, 0 };
const byte exit_room13[] = { N, room14, 0 };
const byte exit_room14[] = { S, room13, W, room15, 0 };
const byte exit_room15[] = { N, room16, E, room14, 0 };
const byte exit_room16[] = { S, room15, W, room17, 0 };
const byte exit_room17[] = { E, room16, S, room18, 0 };
const byte exit_room22[] = { W, room23, 0 };
const byte exit_room23[] = { N, room24, E, room22, 0 };
const byte exit_room24[] = { N, room25, S, room23, 0 };
const byte exit_room25[] = { S, room24, E, room27, W, room26, 0 };
const byte exit_room26[] = { E, room25, 0 };
const byte exit_room27[] = { W, room25, 0 };
const byte exit_room28[] = { W, room23, 0 };

Rooms Room[] =  { /* 0-251 */
/* room0 */
    {
        L"The goats of Mr Seguin liked the open air and freedom... They were all eaten by the wolf.\n\nMr Seguin's Goat\nA text adventure game, based on a story by Alphonse Daudet.\n(c) 2023 Auraes. Type ABOUT for more information.\n\nRelease 1.2 / Serial number 052424 / cQuill",
        NULL
    },
/* room1 */
    {
        L"I'm in a meadow covered with tall bitter grass.\nExit: north",
        exit_room1
    },
/* room2 */
    {
        L"I'm in the dry, stony orchard.\nI can see fruits fallen on the ground.\nExit: east",
        exit_room2
    },
/* room3 */
    {
    L"I'm in the lush grass of a meadow.\nI can see the orchard to the west, and a sign that says: Type READ SIGN, please.\nExits: n, e, w",
        exit_room3
    },
/* room4 */
    {
        L"I'm in a meadow surrounded by hawthorns.\nI can see a large stake stuck in the ground, and the stable to the north.\nExits: n, s, w",
        exit_room4
    },
/* room5 */
    {
        L"I'm in a weedy meadow.\nI can see the vegetable garden to the north.\nExit: north",
        exit_room5
    },
/* room6 */
    {
        L"I'm in a wildflower meadow.\nI can see the farmyard to the north, and the stable to the east.\nExits: n, s, e",
        NULL
    },
/* room7 */
    {
        L"I'm in front of the horse stable.\nI can see the entrance door to the north, and the fenced-in vegetable garden to the east.\nExits: n, s, e, w",
        exit_room7
    },
/* room8 */
    {
        L"I'm in the vegetable garden.\nI can see many varieties of vegetables, a picket fence and the stable to the west.\nExits: s, w",
        NULL
    },
/* room9 */
    {
        L"I'm in the farmyard, behind the stable.\nI can see a small mountain range over the surrounding wall.\nExit: south",
        exit_room9
    },
/* room10 */
    {
        L"I'm in a spacious and airy horse stable.\nExit: south",
        NULL
    },
/* room11 */
    {
        L"I'm at the top of a dead tree.\nI can see a windmill silhouette in the distance, towards the north.\nExit: down",
        exit_room11
    },
/* room12 */
    {
        L"I'm in a double-locked horse stable.\nI can see a large open window at the upper end of the east wall.\nExit: s, e",
        NULL
    },
/* room13 */
    {
        L"I'm beside an old washhouse, at the back of Mr Seguin's house.\nI can see a large weathered stone basin.\nExit: north",
        exit_room13
    },
/* room14 */
    {
        L"I'm on the left bank of a river, crossed by a bridge to the east.\nI can see the washhouse to the south.\nExits: s, e, w",
        exit_room14
    },
/* room15 */
    {
        L"I'm on a small grassy area surrounded by hills.\nI can see a windmill overlooking the landscape to the north, and the river to the east.\nExits: n, e",
        exit_room15
    },
/* room16 */
    {
        L"I'm on top of a windy hill, outside a derelict windmill.\nI can see the front door to the north.\nExits: n, s, w",
        exit_room16
    },
/* room17 */
    {
        L"I'm on a steep path that winds between the rocks.\nI can see a rough trail leading down to the south, and the windmill to the east.\nExits: s, e",
        exit_room17
    },
/* room18 */
    {
        L"I'm on the edge of a precipice overlooking a wide, green valley.\nExit: north",
        NULL
    },
/* room19 */
    {
        L"I'm in a tower-shaped windmill.\nI can see a big pile of flour on a grinding stone.\nExit: south",
        NULL
    },
/* room20 */
    {
        L"I'm in the depths of a dark cave.\nExit: west",
        NULL
    },
/* room21 */
    {
        L"I'm in a rustically furnished cave.\nI can see the outside light coming from the south.\nExits: s, e",
        NULL
    },
/* room22 */
    {
        L"I'm in front of the north entrance to a cave.\nI can see a huge anthill of dried mud and twigs.\nExits: n, w",
        exit_room22
    },
/* room23 */
    {
        L"I'm downstream of the river, on former pastures that have gone wild again.\nI can see a cave to the east, and the north face of a mountain in the distance.\nExits: n, e",
        exit_room23
    },
/* room24 */
    {
        L"I'm on the right bank of a river, crossed by a bridge to the west. \nI can see a path leading up the north side of the mountain.\nExits: n, s, w",
        exit_room24
    },
/* room25 */
    {
        L"I'm on an arid mountain plateau with sparse vegetation.\nI can see a forest to the east, and the lowland to the south.\nExits: s, e, w",
        exit_room25
    },
/* room26 */
    {
        L"I'm on a headland overlooking the valley.\nIn the distance, through the mist, I can see Mr Seguin's house.\nExit: east",
        exit_room26
    },
/* room27 */
    {
        L"I'm in a quiet grove of holm oaks.\nI can see a steep, narrow path rising to the north.\nExits: n, w",
        exit_room27
    },
/* room28 */
    {
        L"It's bright out here! I can't see a thing.",
        exit_room28
    },
/* room29 */
    {
        L"I'm on top of a moonlit mountain, in the menacing shadow of tall, ghostly trees.\nI can see two bright little eyes piercing the night.\n\nWolf: Hi Blanquette, here you're at last!\n\nThe wolf gets up, and howls at the moon, signalling the start of the fight.\n\nIt's showtime!",
        NULL
    },
/* room30 */
    {
        L"I'm on top of a moonlit mountain, in the menacing shadow of tall, ghostly trees.\n\nThe wolf curls up his lips over his fangs, and plays:\n\n The Mongoose card",
        NULL
    },
/* room31 */
    {
        L"I'm on top of a moonlit mountain, in the menacing shadow of tall, ghostly trees.\n\nThe wolf leaps up, sharp claws out, and plays:\n\n The Bird of Prey card",
        NULL
    },
/* room32 */
    {
        L"I'm on top of a moonlit mountain, in the menacing shadoaw of tall, ghostly trees.\n\nThe wolf rolls to the ground, then leaps up, and plays:\n\n The Joker card",
        NULL
    },
/* room33 */
    {
        L"I'm on top of a moonlit mountain, in the menacing shadow of tall, ghostly trees.\n\nThe wolf runs around Blanquette, attacks by surprise, and plays:\n\n The Ectoplasm card",
        NULL
    },
/* room34 */
    {
        L"I'm on top of a moonlit mountain, in the menacing shadow of tall, ghostly trees.\n\nWolf: What a shame, your deck is empty. Ha ha ha! What an unfair world.\n\nThe wolf plays:\n\nThe Angry Wolf card\n\nThe wolf pounces on Blanquette, carries her off, and then eats her.",
        NULL
    },
/* room35 */
    {
        L"Rooster: COCK-A-DOODLE-DO!\n\nCongratulations, you've fought the wolf until dawn.\n\nYOU'VE WON!\n",
        NULL
    },
/* room36 */
    {
        L"You fought well, but the wolf ate you before dawn.\n\nYOU'VE LOST!\n",
        NULL
    }
};

