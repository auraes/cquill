#define CAPACITY 4

Items Item[] = /* 0-254 */
{
/* torch_lit */
    {
        L" A lit *torch*",
        NOTCREATED
    },
/* seguin */
    {
        L" Mr Seguin looking happy",
        room4
    },
/* seguin_cleaning */
    {
        L" Mr Seguin cleaning the stable",
        room10
    },
/* seguin_milking */
    {
        L" Mr Seguin waiting for the milking",
        NOTCREATED
    },
/* seguin_upseting */
    {
        L" Mr Seguin looking upset",
        NOTCREATED
    },
/* ashes */
    {
        L" Ashes of a campfire",
        room25
    },
/* ant */
    {
        L" A worker ant",
        room22
    },
/* boulder */
    {
        L" A large boulder",
        NOTCREATED
    },
/* boulder_1 */
    {
        L" An engraved boulder",
        NOTCREATED
    },
/* campfire */
    {
        L" A campfire smelling of roast lamb",
        NOTCREATED
    },
/* carrot */
    {
        L" A fleshy carrot",
        room8
    },
/* cart */
    {
        L" An overturned hay cart",
        room18
    },
/* chamois */
    {
        L" An alluring chamois",
        NOTCREATED
    },
/* chick */
    {
        L" A cute chick playing hide-and-seek",
        room1
    },
/* chick_1 */
    {
        L" A cute chick",
        NOTCREATED
    },
/* daisy */
    {
        L" A little daisy",
        room6
    },
/* owl */
    {
        L" An old owl perching on the roof frame",
        room19
    },
/* door_locked */
    {
        L" A locked door",
        NOTCREATED
    },
/* door_closed */
    {
        L" A closed door",
        NOTCREATED
    },
/* door_opened */
    {
        L" A wide open door",
        room7
    },
/* barrel */
    {
        L" An empty barrel",
        room10
    },
/* elves */
    {
        L" Elves dancing around a shepherd statue",
        room14
    },
/* statue */
    {
        L" A shepherd statue",
        NOTCREATED
    },
/* fence */
    {
        L" A low picket fence",
        room7
    },
/* hermit */
    {
        L" A blind hermit dressed in a sheepskin",
        room21
    },
/* hermit_1 */
    {
        L" The hermit sleeping soundly",
        NOTCREATED
    },
/* hole */
    {
        L" A small rabbit hole",
        room18
    },
/* hole_1 */
    {
        L" A wide and deep rabbit hole",
        NOTCREATED
    },
/* kitten */
    {
        L" Kitten scanning the meadow for a chick",
        room11
    },
/* kitten_asleep */
    {
        L" Kitten sleeping on an empty barrel",
        room12
    },
/* lamb */
    {
        L" A lamb quenching its thirst",
        room23
    },
/* lamb_1 */
    {
        L" An anxious lamb",
        NOTCREATED
    },
/* mule_fly */
    {
        L" A mule keeping flies away with his tail",
        room15
    },
/* mule */
    {
        L" A mule grazing peacefully",
        NOTCREATED
    },
/* mule_cart */
    {
        L" A mule hitched to a cart",
        NOTCREATED
    },
/* puddle */
    {
        L" A puddle of water reflecting the sky",
        room23
    },
/* rabbit_fearful */
    {
        L" A fearful rabbit",
        NOTCREATED
    },
/* rabbit_leisurely */
    {
        L" A leisurely rabbit",
        room16
    },
/* rooster */
    {
        L" A young rooster puffing out his chest",
        room9
    },
/* rooster_1 */
    {
        L" A young rooster",
        NOTCREATED
    },
/* rope */
    {
        L" A long rope tied to a stake",
        room4
    },
/* salamander */
    {
        L" A yellow-spotted salamander",
        room13
    },
/* salt_stone */
    {
        L" A salt stone fixed to the wall",
        room10
    },
/* sign */
    {
        L" A warning sign.",
        room24
    },
/* skeleton */
    {
        L" The Renaude goat's skeleton",
        room27
    },
/* snake */
    {
        L" A poisonous snake standing upright",
        room17
    },
/* tree */
    {
        L" A tree struck by lightning\n The shadow of a perched animal",
        room2
    },
/* tree_1 */
    {
        L" A tree struck by lightning",
        NOTCREATED
    },
/* track */
    {
        L" An animal's tracks",
        room26
    },
/* almond */
    {
        L" A soft-shelled almond",
        room2
    },
/* bag_empty */
    {
        L" An empty *bag*",
        room19
    },
/* bag_flour */
    {
        L" A *bag* full of flour",
        NOTCREATED
    },
/* card0 */
    {
        L" A bloody *card*",
        NOTCREATED
    },
/* card1 */
    {
        L" The Four-leaf Clover *card*",
        NOTCREATED
    },
/* card2 */
    {
        L" The Toadstool *card*",
        room5
    },
/* card3 */
    {
        L" The Venomous Snake *card*",
        NOTCREATED
    },
/* card4 */
    {
        L" The Kitten *card*",
        NOTCREATED
    },
/* card5 */
    {
        L" The Morning Star *card*",
        room20
    },
/* bone */
    {
        L" A broken *bone*",
        NOTCREATED
    },
/* book */
    {
        L" A *cookbook*",
        room25
    },
/* dung */
    {
        L" A mule *dung*",
        room18
    },
/* dung_fly */
    {
        L" A *dung* full of flies",
        NOTCREATED
    },
/* key */
    {
        L" A rusty *key*",
        room13
    },
/* key_broken */
    {
        L" A broken *key*",
        NOTCREATED
    },
/* tambourine */
    {
        L" An headless *tambourine*",
        room14
    },
/* sign_broken */
    {
        L" A broken wooden *sign*",
        NOTCREATED
    },
/* torch_unlit */
    {
        L" An unlit *torch*",
        room21
    },
/* deck */
    {
        L" A deck of playing cards",
        CARRIED
    },
/* barrel_1 */
    {
        L" A barrel out of place",
        NOTCREATED
   }
};
