#define FLAG_DARK        0
#define FLAG_YESNO       5
#define FLAG_PUTIN       6
#define FLAG_START       11
#define FLAG_CORDE       12
#define FLAG_EAT         13
#define FLAG_OBEDIENT    14
#define FLAG_SEGUIN      15
#define FLAG_LISTEN      16
#define FLAG_PLAY        19
#define FLAG_TURN        20
#define FLAG_MAX         21
#define FLAG_INV         22
#define FLAG_SCORE_PLUS  23
#define FLAG_NCARDS      24
#define FLAG_CARD1       25
#define FLAG_CARD2       26
#define FLAG_CARD3       27
#define FLAG_CARD4       28
#define FLAG_CARD5       29
#define FLAG_SCORE       30
#define FLAG_BARREL      12
#define FLAG_SALT        13
#define FLAG_LICK        14
#define FLAG_SHORTCUT    12
#define FLAG_RABBIT      13
#define FLAG_SNAKE       14
#define FLAG_JINGLE      15
#define FLAG_STORM       16
#define FLAG_BOULDER     13
#define FLAG_PATH        14
#define FLAG_NOBODY      15
#define FLAG_CAMPFIRE    16
#define FLAG_LAMB        17

const byte eNone[] = {

	0
};

const byte eAny[] = {

	0
};

const byte eStatus[] = {

	12,
	At, room0,
	Set, FLAG_CORDE,
	Let, FLAG_EAT, 4,
	Anykey,
	Goto, room4,
	Desc,

	14,
	At, room4,
	Zero, FLAG_START,
	Let, FLAG_START, 1,
	Destroy, seguin,
	Message, 0,
	Anykey,
	Desc,

	10,
	At, room4,
	Eq, FLAG_START, 1,
	Set, FLAG_START,
	Message, 1,

	7,
	At, room4,
	Notzero, FLAG_CORDE,
	Set, FLAG_CORDE,

	11,
	At, room10,
	Notzero, FLAG_CORDE,
	Message, 2,
	Anykey,
	Goto, room7,
	Desc,

	12,
	Eq, FLAG_EAT, 1,
	Clear, FLAG_EAT,
	Place, seguin_milking, room4,
	Swap, door_opened, door_locked,

	24,
	At, room12,
	Notzero, FLAG_BARREL,
	Clear, FLAG_SHORTCUT,
	Clear, FLAG_RABBIT,
	Clear, FLAG_SNAKE,
	Clear, FLAG_JINGLE,
	Clear, FLAG_LISTEN,
	Place, door_locked, room16,
	Message, 3,
	Anykey,
	Goto, room13,
	Desc,

	8,
	At, room16,
	Zero, FLAG_SNAKE,
	Place, snake, room17,

	8,
	At, room18,
	Zero, FLAG_SNAKE,
	Place, snake, room17,

	8,
	At, room18,
	Zero, FLAG_RABBIT,
	Place, rabbit_leisurely, room16,

	16,
	Eq, FLAG_STORM, 1,
	At, room16,
	Set, FLAG_STORM,
	Swap, elves, statue,
	Cls,
	Message, 4,
	Anykey,
	Desc,

	19,
	At, room21,
	Eq, FLAG_CAMPFIRE, 3,
	Clear, FLAG_CAMPFIRE,
	Destroy, hermit,
	Swap, ashes, campfire,
	Destroy, lamb_1,
	Message, 5,
	Anykey,
	Desc,

	8,
	At, room29,
	Eq, FLAG_NCARDS, 4,
	Set, FLAG_MAX,

	9,
	At, room29,
	Goto, room30,
	Set, FLAG_TURN,
	Anykey,
	Desc,

	9,
	Atgt, room29,
	Notzero, FLAG_TURN,
	Clear, FLAG_TURN,
	Message, 6,

	10,
	At, room30,
	Eq, FLAG_PLAY, 1,
	Message, 7,
	Message, 8,

	10,
	At, room30,
	Eq, FLAG_PLAY, 2,
	Message, 9,
	Message, 10,

	10,
	At, room30,
	Eq, FLAG_PLAY, 3,
	Message, 11,
	Message, 10,

	10,
	At, room30,
	Eq, FLAG_PLAY, 4,
	Message, 12,
	Message, 13,

	13,
	At, room30,
	Notzero, FLAG_PLAY,
	Clear, FLAG_PLAY,
	Set, FLAG_TURN,
	Goto, room31,
	Anykey,
	Desc,

	10,
	At, room31,
	Eq, FLAG_PLAY, 1,
	Message, 14,
	Message, 13,

	10,
	At, room31,
	Eq, FLAG_PLAY, 2,
	Message, 15,
	Message, 8,

	10,
	At, room31,
	Eq, FLAG_PLAY, 3,
	Message, 11,
	Message, 10,

	10,
	At, room31,
	Eq, FLAG_PLAY, 4,
	Message, 16,
	Message, 17,

	13,
	At, room31,
	Notzero, FLAG_PLAY,
	Clear, FLAG_PLAY,
	Set, FLAG_TURN,
	Goto, room32,
	Anykey,
	Desc,

	10,
	At, room32,
	Eq, FLAG_PLAY, 1,
	Message, 18,
	Message, 8,

	10,
	At, room32,
	Eq, FLAG_PLAY, 2,
	Message, 18,
	Message, 8,

	10,
	At, room32,
	Eq, FLAG_PLAY, 3,
	Message, 18,
	Message, 8,

	10,
	At, room32,
	Eq, FLAG_PLAY, 4,
	Message, 18,
	Message, 8,

	13,
	At, room32,
	Notzero, FLAG_PLAY,
	Clear, FLAG_PLAY,
	Set, FLAG_TURN,
	Goto, room33,
	Anykey,
	Desc,

	10,
	At, room33,
	Eq, FLAG_PLAY, 1,
	Message, 19,
	Message, 10,

	10,
	At, room33,
	Eq, FLAG_PLAY, 2,
	Message, 20,
	Message, 13,

	10,
	At, room33,
	Eq, FLAG_PLAY, 3,
	Message, 11,
	Message, 10,

	10,
	At, room33,
	Eq, FLAG_PLAY, 4,
	Message, 21,
	Message, 8,

	9,
	At, room33,
	Notzero, FLAG_PLAY,
	Goto, room34,
	Anykey,
	Desc,

	10,
	Atgt, room29,
	Zero, FLAG_NCARDS,
	Set, FLAG_NCARDS,
	Goto, room34,
	Desc,

	9,
	At, room34,
	Notzero, FLAG_MAX,
	Goto, room35,
	Anykey,
	Desc,

	7,
	At, room34,
	Goto, room36,
	Anykey,
	Desc,

	6,
	At, room35,
	Score,
	Turns,
	End,

	6,
	At, room36,
	Score,
	Turns,
	End,

	10,
	Notzero, FLAG_SCORE_PLUS,
	Clear, FLAG_SCORE_PLUS,
	Message, 22,
	Plus, FLAG_SCORE, 10,

	8,
	Eq, FLAG_INV, 1,
	Set, FLAG_INV,
	Message, 23,

	0
};

const byte eN[] = {

	20,
	ANY,
	Notzero, FLAG_YESNO,
	At, room11,
	Present, kitten,
	Destroy, kitten,
	Swap, chick, chick_1,
	Swap, tree, tree_1,
	Message, 24,
	Anykey,
	Desc,

	11,
	ANY,
	Notzero, FLAG_YESNO,
	At, room8,
	Present, seguin_upseting,
	Message, 25,
	Done,

	9,
	ANY,
	Notzero, FLAG_YESNO,
	At, room18,
	Present, hole_1,
	Done,

	9,
	ANY,
	At, room6,
	Notzero, FLAG_CORDE,
	Message, 26,
	Done,

	7,
	ANY,
	At, room6,
	Goto, room9,
	Desc,

	9,
	ANY,
	At, room7,
	Present, door_opened,
	Goto, room10,
	Desc,

	7,
	ANY,
	At, room7,
	Message, 27,
	Done,

	12,
	ANY,
	At, room16,
	Present, door_opened,
	Place, door_opened, room19,
	Goto, room19,
	Desc,

	7,
	ANY,
	At, room16,
	Message, 27,
	Done,

	7,
	ANY,
	At, room18,
	Goto, room17,
	Desc,

	9,
	ANY,
	At, room22,
	Present, boulder_1,
	Message, 28,
	Done,

	7,
	ANY,
	At, room22,
	Goto, room21,
	Desc,

	9,
	ANY,
	At, room28,
	Present, boulder_1,
	Message, 28,
	Done,

	7,
	ANY,
	At, room28,
	Goto, room21,
	Desc,

	23,
	ANY,
	At, room27,
	Zero, FLAG_PATH,
	Set, FLAG_PATH,
	Swap, track, chamois,
	Place, boulder, room24,
	Swap, sign, sign_broken,
	Create, bone,
	Message, 29,
	Anykey,
	Desc,

	7,
	ANY,
	At, room27,
	Message, 30,
	Done,

	0
};

const byte eS[] = {

	10,
	ANY,
	At, room6,
	Eq, FLAG_CORDE, 255,
	Message, 26,
	Done,

	7,
	ANY,
	At, room6,
	Goto, room3,
	Desc,

	9,
	ANY,
	At, room8,
	Notzero, FLAG_CORDE,
	Message, 26,
	Done,

	7,
	ANY,
	At, room8,
	Goto, room5,
	Desc,

	7,
	ANY,
	At, room12,
	Message, 31,
	Done,

	12,
	ANY,
	At, room19,
	Present, door_opened,
	Place, door_opened, room16,
	Goto, room16,
	Desc,

	7,
	ANY,
	At, room19,
	Message, 27,
	Done,

	9,
	ANY,
	At, room21,
	Notzero, FLAG_BOULDER,
	Message, 28,
	Done,

	7,
	ANY,
	At, room21,
	Goto, room28,
	Desc,

	7,
	ANY,
	Atgt, room28,
	Message, 32,
	Done,

	0
};

const byte eE[] = {

	10,
	ANY,
	At, room6,
	Eq, FLAG_CORDE, 1,
	Message, 26,
	Done,

	7,
	ANY,
	At, room6,
	Goto, room7,
	Desc,

	7,
	ANY,
	At, room7,
	Message, 33,
	Done,

	7,
	ANY,
	Present, barrel_1,
	Set, FLAG_BARREL,
	Done,

	7,
	ANY,
	At, room12,
	Message, 34,
	Done,

	9,
	ANY,
	Present, elves,
	Message, 35,
	Message, 36,
	Done,

	7,
	ANY,
	At, room14,
	Message, 37,
	Done,

	13,
	ANY,
	At, room18,
	Zero, FLAG_SHORTCUT,
	Set, FLAG_SHORTCUT,
	Set, FLAG_SCORE_PLUS,
	Message, 38,
	Anykey,

	7,
	ANY,
	At, room18,
	Goto, room15,
	Desc,

	9,
	ANY,
	At, room21,
	Set, FLAG_DARK,
	Goto, room20,
	Desc,

	13,
	ANY,
	At, room24,
	Zero, FLAG_SHORTCUT,
	Set, FLAG_SHORTCUT,
	Set, FLAG_SCORE_PLUS,
	Message, 39,
	Anykey,

	7,
	ANY,
	At, room24,
	Goto, room21,
	Desc,

	0
};

const byte eW[] = {

	9,
	ANY,
	At, room4,
	Notzero, FLAG_CORDE,
	Let, FLAG_CORDE, 1,

	7,
	ANY,
	At, room4,
	Goto, room3,
	Desc,

	7,
	ANY,
	At, room8,
	Message, 33,
	Done,

	13,
	ANY,
	At, room15,
	Zero, FLAG_SHORTCUT,
	Set, FLAG_SHORTCUT,
	Set, FLAG_SCORE_PLUS,
	Message, 38,
	Anykey,

	7,
	ANY,
	At, room15,
	Goto, room18,
	Desc,

	9,
	ANY,
	At, room20,
	Clear, FLAG_DARK,
	Goto, room21,
	Desc,

	13,
	ANY,
	At, room21,
	Zero, FLAG_SHORTCUT,
	Set, FLAG_SHORTCUT,
	Set, FLAG_SCORE_PLUS,
	Message, 39,
	Anykey,

	7,
	ANY,
	At, room21,
	Goto, room24,
	Desc,

	7,
	ANY,
	At, room24,
	Message, 37,
	Done,

	0
};

const byte eU[] = {

	7,
	BARREL,
	Present, kitten_asleep,
	Message, 40,
	Done,

	7,
	BARREL,
	Present, barrel,
	Message, 41,
	Done,

	7,
	BARREL,
	Present, barrel_1,
	Set, FLAG_BARREL,
	Done,

	7,
	WINDOW,
	Present, barrel_1,
	Set, FLAG_BARREL,
	Done,

	7,
	WINDOW,
	At, room12,
	Message, 34,
	Done,

	7,
	RIVER,
	At, room14,
	Message, 42,
	Done,

	7,
	RIVER,
	At, room24,
	Message, 42,
	Done,

	7,
	ANY,
	At, room7,
	Goto, room8,
	Desc,

	7,
	ANY,
	At, room8,
	Goto, room7,
	Desc,

	9,
	ANY,
	At, room2,
	Notzero, FLAG_CORDE,
	Message, 26,
	Done,

	7,
	ANY,
	At, room2,
	Goto, room11,
	Desc,

	5,
	ANY,
	Message, 43,
	Done,

	0
};

const byte eEnter[] = {

	7,
	HOLE,
	Present, cart,
	Message, 44,
	Done,

	7,
	HOLE,
	Present, hole,
	Message, 45,
	Done,

	10,
	HOLE,
	Present, hole_1,
	Message, 46,
	Let, FLAG_YESNO, 2,
	Done,

	0
};

const byte eRun[] = {

	7,
	ANY,
	Atgt, room28,
	Message, 32,
	Done,

	0
};

const byte eX[] = {

	7,
	KEY,
	Present, salamander,
	Message, 47,
	Done,

	7,
	KEY,
	Present, key,
	Message, 48,
	Done,

	7,
	KEY,
	Present, key_broken,
	Message, 49,
	Done,

	7,
	TAMBOURINE,
	Present, tambourine,
	Message, 50,
	Done,

	7,
	DUNG,
	Present, dung,
	Message, 51,
	Done,

	7,
	DUNG,
	Present, dung_fly,
	Message, 52,
	Done,

	7,
	BAG,
	Present, owl,
	Message, 53,
	Done,

	7,
	BAG,
	Present, bag_empty,
	Message, 54,
	Done,

	7,
	BAG,
	Present, bag_flour,
	Message, 55,
	Done,

	7,
	TORCH,
	Present, torch_lit,
	Message, 56,
	Done,

	7,
	TORCH,
	Present, torch_unlit,
	Message, 57,
	Done,

	7,
	BOOK,
	Present, book,
	Message, 58,
	Done,

	7,
	BONE,
	Present, bone,
	Message, 59,
	Done,

	7,
	SIGN,
	Present, sign_broken,
	Message, 60,
	Done,

	6,
	DECK,
	Set, FLAG_INV,
	Message, 61,

	6,
	DECK,
	Notzero, FLAG_CARD4,
	Message, 62,

	6,
	DECK,
	Notzero, FLAG_CARD2,
	Message, 63,

	6,
	DECK,
	Notzero, FLAG_CARD1,
	Message, 64,

	6,
	DECK,
	Notzero, FLAG_CARD3,
	Message, 65,

	6,
	DECK,
	Notzero, FLAG_CARD5,
	Message, 66,

	3,
	DECK,
	Done,

	7,
	CARD,
	Present, card0,
	Message, 67,
	Done,

	7,
	CARD,
	Present, card1,
	Message, 68,
	Done,

	7,
	CARD,
	Present, card2,
	Message, 68,
	Done,

	7,
	CARD,
	Present, card3,
	Message, 68,
	Done,

	7,
	CARD,
	Present, card4,
	Message, 68,
	Done,

	7,
	CARD,
	Present, card5,
	Message, 69,
	Done,

	5,
	ANY,
	Message, 70,
	Done,

	0
};

const byte eRead[] = {

	5,
	RULE,
	Message, 71,
	Done,

	10,
	SIGN,
	At, room3,
	Zero, FLAG_OBEDIENT,
	Set, FLAG_OBEDIENT,
	Set, FLAG_SCORE_PLUS,

	7,
	SIGN,
	At, room3,
	Message, 72,
	Done,

	9,
	SIGN,
	Present, sign,
	At, room24,
	Message, 73,
	Done,

	11,
	BOOK,
	Present, book,
	Drop, book,
	Get, book,
	Message, 74,
	Done,

	7,
	BOULDER,
	Present, boulder_1,
	Message, 75,
	Done,

	0
};

const byte eGet[] = {

	7,
	CARD,
	Present, card0,
	Message, 76,
	Done,

	12,
	CARD,
	Present, card1,
	Plus, FLAG_NCARDS, 1,
	Destroy, card1,
	Set, FLAG_CARD1,
	Desc,

	12,
	CARD,
	Present, card2,
	Plus, FLAG_NCARDS, 1,
	Destroy, card2,
	Set, FLAG_CARD2,
	Desc,

	12,
	CARD,
	Present, card3,
	Plus, FLAG_NCARDS, 1,
	Destroy, card3,
	Set, FLAG_CARD3,
	Desc,

	12,
	CARD,
	Present, card4,
	Plus, FLAG_NCARDS, 1,
	Destroy, card4,
	Set, FLAG_CARD4,
	Desc,

	13,
	CARD,
	Present, card5,
	Present, torch_lit,
	Create, torch_lit,
	Destroy, card5,
	Set, FLAG_CARD5,
	Desc,

	11,
	KEY,
	At, room13,
	Present, salamander,
	Message, 77,
	Message, 78,
	Done,

	9,
	KEY,
	Present, key_broken,
	Notcarr, key_broken,
	Message, 79,
	Done,

	7,
	KEY,
	Present, key,
	Get, key,
	Desc,

	5,
	KEY,
	Get, key_broken,
	Desc,

	7,
	TAMBOURINE,
	Present, elves,
	Message, 80,
	Done,

	5,
	TAMBOURINE,
	Get, tambourine,
	Desc,

	9,
	DUNG,
	At, room13,
	Absent, salamander,
	Message, 79,
	Done,

	7,
	DUNG,
	Present, dung,
	Get, dung,
	Desc,

	5,
	DUNG,
	Get, dung_fly,
	Desc,

	7,
	BAG,
	Present, owl,
	Message, 77,
	Done,

	7,
	BAG,
	Present, bag_empty,
	Get, bag_empty,
	Desc,

	5,
	BAG,
	Get, bag_flour,
	Desc,

	9,
	TORCH,
	Present, torch_unlit,
	Present, hermit,
	Message, 81,
	Done,

	9,
	TORCH,
	At, room20,
	Present, torch_lit,
	Message, 79,
	Done,

	7,
	TORCH,
	Present, torch_unlit,
	Get, torch_unlit,
	Desc,

	5,
	TORCH,
	Get, torch_lit,
	Desc,

	5,
	BOOK,
	Get, book,
	Desc,

	11,
	BONE,
	Notzero, FLAG_BOULDER,
	Present, bone,
	Notcarr, bone,
	Message, 79,
	Done,

	5,
	BONE,
	Get, bone,
	Desc,

	9,
	SIGN,
	Present, sign_broken,
	Present, boulder,
	Message, 82,
	Done,

	7,
	SIGN,
	Present, sign_broken,
	Get, sign_broken,
	Desc,

	5,
	ANY,
	Message, 83,
	Done,

	0
};

const byte eDrop[] = {

	5,
	DECK,
	Message, 84,
	Done,

	7,
	KEY,
	Present, key,
	Drop, key,
	Desc,

	5,
	KEY,
	Drop, key_broken,
	Desc,

	5,
	TAMBOURINE,
	Drop, tambourine,
	Desc,

	14,
	DUNG,
	Carried, dung,
	Present, mule_fly,
	Swap, mule_fly, mule,
	Destroy, dung,
	Create, dung_fly,
	Desc,

	7,
	DUNG,
	Present, dung,
	Drop, dung,
	Desc,

	5,
	DUNG,
	Drop, dung_fly,
	Desc,

	7,
	BAG,
	Present, bag_empty,
	Drop, bag_empty,
	Desc,

	5,
	BAG,
	Drop, bag_flour,
	Desc,

	12,
	TORCH,
	At, room20,
	Present, torch_unlit,
	Drop, torch_unlit,
	Place, torch_unlit, room22,
	Ok,

	12,
	TORCH,
	At, room28,
	Present, torch_unlit,
	Drop, torch_unlit,
	Place, torch_unlit, room22,
	Ok,

	7,
	TORCH,
	Present, torch_unlit,
	Drop, torch_unlit,
	Desc,

	10,
	TORCH,
	At, room28,
	Drop, torch_lit,
	Place, torch_lit, room22,
	Ok,

	5,
	TORCH,
	Drop, torch_lit,
	Desc,

	10,
	BOOK,
	At, room20,
	Drop, book,
	Place, book, room22,
	Ok,

	10,
	BOOK,
	At, room28,
	Drop, book,
	Place, book, room22,
	Ok,

	5,
	BOOK,
	Drop, book,
	Desc,

	10,
	BONE,
	At, room20,
	Drop, bone,
	Place, bone, room22,
	Ok,

	10,
	BONE,
	At, room28,
	Drop, bone,
	Place, bone, room22,
	Ok,

	5,
	BONE,
	Drop, bone,
	Desc,

	12,
	SIGN,
	Present, sign_broken,
	At, room20,
	Drop, sign_broken,
	Place, sign_broken, room22,
	Ok,

	7,
	SIGN,
	Present, sign_broken,
	Drop, sign_broken,
	Desc,

	5,
	SIGN,
	Drop, sign,
	Desc,

	0
};

const byte eFill[] = {

	9,
	BAG,
	At, room19,
	Present, bag_flour,
	Message, 85,
	Done,

	16,
	BAG,
	At, room19,
	Present, bag_empty,
	Drop, bag_empty,
	Get, bag_empty,
	Swap, bag_empty, bag_flour,
	Message, 86,
	Done,

	13,
	BAG,
	Notat, room19,
	Present, bag_empty,
	Drop, bag_empty,
	Get, bag_empty,
	Message, 87,
	Done,

	0
};

const byte eEmpty[] = {

	7,
	BAG,
	Present, bag_empty,
	Message, 88,
	Done,

	12,
	BAG,
	Present, bag_flour,
	Drop, bag_flour,
	Get, bag_flour,
	Swap, bag_flour, bag_empty,
	Ok,

	0
};

const byte eWear[] = {

	9,
	TAMBOURINE,
	Drop, tambourine,
	Get, tambourine,
	Message, 89,
	Done,

	0
};

const byte eGive[] = {

	5,
	DECK,
	Message, 84,
	Done,

	20,
	DUNG,
	Present, dung_fly,
	Present, salamander,
	Drop, dung_fly,
	Get, dung_fly,
	Destroy, salamander,
	Destroy, dung_fly,
	Create, dung,
	Message, 90,
	Anykey,
	Desc,

	26,
	TAMBOURINE,
	Present, mule,
	Drop, tambourine,
	Get, tambourine,
	Set, FLAG_SNAKE,
	Swap, snake, card3,
	Destroy, mule,
	Destroy, cart,
	Destroy, tambourine,
	Place, mule_cart, room16,
	Message, 91,
	Anykey,
	Desc,

	11,
	TAMBOURINE,
	Present, snake,
	Drop, tambourine,
	Get, tambourine,
	Message, 92,
	Done,

	13,
	BAG,
	Present, bag_empty,
	Present, mule_cart,
	Drop, bag_empty,
	Get, bag_empty,
	Message, 93,
	Done,

	11,
	BAG,
	Present, mule_cart,
	Drop, bag_flour,
	Get, bag_flour,
	Message, 94,
	Done,

	14,
	BOOK,
	Present, hermit,
	Drop, book,
	Plus, FLAG_CAMPFIRE, 1,
	Destroy, book,
	Message, 95,
	Done,

	14,
	SIGN,
	Present, hermit,
	Drop, sign_broken,
	Plus, FLAG_CAMPFIRE, 1,
	Destroy, sign_broken,
	Message, 95,
	Done,

	5,
	ANY,
	Message, 96,
	Done,

	0
};

const byte ePut[] = {

	14,
	BAG,
	Present, bag_empty,
	Drop, bag_empty,
	Get, bag_empty,
	Let, FLAG_PUTIN, 2,
	Message, 97,
	Done,

	12,
	BAG,
	Drop, bag_flour,
	Get, bag_flour,
	Let, FLAG_PUTIN, 2,
	Message, 97,
	Done,

	0
};

const byte eCart[] = {

	11,
	ANY,
	Notzero, FLAG_PUTIN,
	Carried, bag_empty,
	Present, mule_cart,
	Message, 93,
	Done,

	16,
	ANY,
	Notzero, FLAG_PUTIN,
	Present, mule_cart,
	Destroy, bag_flour,
	Destroy, mule_cart,
	Set, FLAG_SCORE_PLUS,
	Message, 98,
	Anykey,
	Desc,

	0
};

const byte eTalk[] = {

	12,
	KITTEN,
	At, room11,
	Present, kitten,
	Let, FLAG_YESNO, 2,
	Message, 99,
	Done,

	14,
	SEGUIN,
	Present, seguin_milking,
	Clear, FLAG_CORDE,
	Destroy, rope,
	Destroy, seguin_milking,
	Message, 100,
	Anykey,
	Desc,

	10,
	SEGUIN,
	Present, seguin_upseting,
	Zero, FLAG_SEGUIN,
	Set, FLAG_SEGUIN,
	Message, 101,

	10,
	SEGUIN,
	Present, seguin_upseting,
	Message, 102,
	Let, FLAG_YESNO, 2,
	Done,

	18,
	ROOSTER,
	Present, rooster,
	Swap, rooster, rooster_1,
	Place, seguin_upseting, room8,
	Message, 103,
	Message, 104,
	Message, 105,
	Anykey,
	Desc,

	7,
	ROOSTER,
	Present, rooster_1,
	Message, 106,
	Done,

	7,
	CHICK,
	Present, chick,
	Message, 107,
	Done,

	7,
	CHICK,
	Present, chick_1,
	Message, 108,
	Done,

	7,
	SALAMANDER,
	Present, salamander,
	Message, 109,
	Done,

	9,
	ELVES,
	Present, elves,
	Message, 110,
	Message, 36,
	Done,

	7,
	MULE,
	Present, mule_fly,
	Message, 111,
	Done,

	7,
	MULE,
	Present, mule,
	Message, 111,
	Done,

	7,
	MULE,
	Present, mule_cart,
	Message, 112,
	Done,

	7,
	SNAKE,
	Present, snake,
	Message, 113,
	Done,

	7,
	OWL,
	Present, owl,
	Message, 114,
	Done,

	17,
	RABBIT,
	Present, rabbit_leisurely,
	Present, door_opened,
	Destroy, rabbit_leisurely,
	Place, rabbit_fearful, room19,
	Set, FLAG_RABBIT,
	Message, 115,
	Anykey,
	Desc,

	10,
	RABBIT,
	Present, rabbit_leisurely,
	Destroy, rabbit_leisurely,
	Message, 116,
	Anykey,
	Desc,

	14,
	RABBIT,
	Present, rabbit_fearful,
	Present, door_opened,
	Destroy, rabbit_fearful,
	Clear, FLAG_RABBIT,
	Message, 116,
	Anykey,
	Desc,

	18,
	RABBIT,
	Present, rabbit_fearful,
	Destroy, rabbit_fearful,
	Destroy, owl,
	Place, card1, room16,
	Let, FLAG_STORM, 1,
	Message, 117,
	Anykey,
	Desc,

	15,
	HERMIT,
	At, room21,
	Present, hermit,
	Zero, FLAG_NOBODY,
	Set, FLAG_NOBODY,
	Message, 118,
	Message, 119,
	Done,

	9,
	HERMIT,
	At, room21,
	Present, hermit,
	Message, 118,
	Done,

	7,
	ANT,
	Present, ant,
	Message, 120,
	Done,

	10,
	LAMB,
	Present, lamb,
	Zero, FLAG_LAMB,
	Set, FLAG_LAMB,
	Set, FLAG_SCORE_PLUS,

	7,
	LAMB,
	Present, lamb,
	Message, 121,
	Done,

	7,
	LAMB,
	Present, lamb_1,
	Message, 122,
	Done,

	17,
	CHAMOIS,
	Present, chamois,
	Notzero, FLAG_CARD5,
	Dropall,
	Get, deck,
	Clear, FLAG_CARD5,
	Goto, room29,
	Message, 123,
	Anykey,
	Desc,

	7,
	CHAMOIS,
	Present, chamois,
	Message, 124,
	Done,

	11,
	MOTH,
	Notzero, FLAG_BOULDER,
	At, room20,
	Absent, torch_lit,
	Message, 125,
	Done,

	9,
	MOTH,
	At, room20,
	Absent, torch_lit,
	Message, 126,
	Done,

	5,
	ANY,
	Message, 127,
	Done,

	0
};

const byte eWake[] = {

	13,
	KITTEN,
	Present, kitten_asleep,
	Notzero, FLAG_LICK,
	Swap, kitten_asleep, barrel,
	Message, 128,
	Anykey,
	Desc,

	7,
	KITTEN,
	Present, kitten_asleep,
	Message, 129,
	Done,

	7,
	HERMIT,
	Present, hermit_1,
	Message, 130,
	Done,

	0
};

const byte eEat[] = {

	12,
	DAISY,
	Present, daisy,
	Destroy, daisy,
	Minus, FLAG_EAT, 1,
	Set, FLAG_SCORE_PLUS,
	Desc,

	12,
	CARROT,
	Present, carrot,
	Destroy, carrot,
	Minus, FLAG_EAT, 1,
	Set, FLAG_SCORE_PLUS,
	Desc,

	12,
	ALMOND,
	Present, almond,
	Destroy, almond,
	Minus, FLAG_EAT, 1,
	Set, FLAG_SCORE_PLUS,
	Desc,

	7,
	SALT,
	Present, salt_stone,
	Message, 131,
	Done,

	5,
	ANY,
	Message, 132,
	Done,

	0
};

const byte eLick[] = {

	9,
	SALT,
	Present, salt_stone,
	Set, FLAG_SALT,
	Message, 133,
	Done,

	8,
	KITTEN,
	Present, kitten_asleep,
	Notzero, FLAG_SALT,
	Set, FLAG_LICK,

	7,
	KITTEN,
	Present, kitten_asleep,
	Message, 134,
	Done,

	0
};

const byte eDrink[] = {

	7,
	ANY,
	At, room14,
	Message, 135,
	Done,

	7,
	ANY,
	At, room24,
	Message, 135,
	Done,

	18,
	ANY,
	At, room23,
	Present, lamb,
	Message, 136,
	Destroy, lamb,
	Place, lamb_1, room21,
	Plus, FLAG_CAMPFIRE, 1,
	Anykey,
	Desc,

	7,
	ANY,
	At, room23,
	Message, 135,
	Done,

	5,
	ANY,
	Message, 137,
	Done,

	0
};

const byte eOpen[] = {

	7,
	DOOR,
	Present, door_opened,
	Message, 138,
	Done,

	7,
	DOOR,
	Present, door_locked,
	Message, 31,
	Done,

	8,
	DOOR,
	Present, door_closed,
	Swap, door_closed, door_opened,
	Desc,

	0
};

const byte eClose[] = {

	7,
	DOOR,
	Present, door_closed,
	Message, 139,
	Done,

	7,
	DOOR,
	Present, door_locked,
	Message, 139,
	Done,

	9,
	DOOR,
	At, room7,
	Present, door_opened,
	Message, 140,
	Done,

	8,
	DOOR,
	Present, door_opened,
	Swap, door_opened, door_closed,
	Desc,

	0
};

const byte eLock[] = {

	7,
	DOOR,
	Present, door_locked,
	Message, 141,
	Done,

	7,
	DOOR,
	Present, door_opened,
	Message, 142,
	Done,

	9,
	DOOR,
	Present, door_closed,
	Notcarr, key,
	Message, 143,
	Done,

	8,
	DOOR,
	Present, door_closed,
	Swap, door_closed, door_locked,
	Desc,

	0
};

const byte eUnlock[] = {

	9,
	DOOR,
	Present, door_locked,
	Notcarr, key,
	Message, 143,
	Done,

	13,
	DOOR,
	At, room16,
	Present, door_locked,
	Swap, door_locked, door_closed,
	Swap, key, key_broken,
	Desc,

	0
};

const byte eRub[] = {

	8,
	CARD,
	Present, card0,
	Swap, card0, card4,
	Desc,

	5,
	ANY,
	Message, 144,
	Done,

	0
};

const byte eBurn[] = {

	20,
	TORCH,
	Present, torch_unlit,
	Present, campfire,
	Drop, torch_unlit,
	Get, torch_unlit,
	Swap, torch_unlit, torch_lit,
	Place, hermit_1, room21,
	Swap, campfire, ashes,
	Desc,

	7,
	TORCH,
	Present, torch_unlit,
	Message, 145,
	Done,

	7,
	SKELETON,
	Present, skeleton,
	Message, 146,
	Done,

	5,
	ANY,
	Message, 147,
	Done,

	0
};

const byte eExtinguish[] = {

	7,
	TORCH,
	Present, torch_lit,
	Message, 148,
	Done,

	7,
	CAMPFIRE,
	Present, campfire,
	Message, 148,
	Done,

	0
};

const byte eBreak[] = {

	7,
	ROPE,
	Present, rope,
	Message, 149,
	Done,

	5,
	ANY,
	Message, 150,
	Done,

	0
};

const byte eAttack[] = {

	7,
	ANY,
	Atgt, room29,
	Message, 151,
	Done,

	5,
	ANY,
	Message, 152,
	Done,

	0
};

const byte eMove[] = {

	7,
	CART,
	Present, cart,
	Message, 153,
	Done,

	7,
	STATUE,
	Present, statue,
	Message, 154,
	Done,

	7,
	BARREL,
	Present, kitten_asleep,
	Message, 40,
	Done,

	13,
	BARREL,
	Present, barrel,
	Absent, barrel_1,
	Swap, barrel, barrel_1,
	Message, 155,
	Anykey,
	Desc,

	7,
	KITTEN,
	Present, kitten_asleep,
	Message, 152,
	Done,

	7,
	BOULDER,
	Present, boulder,
	Message, 154,
	Done,

	7,
	BOULDER,
	Present, boulder_1,
	Message, 154,
	Done,

	9,
	BOULDER,
	At, room21,
	Notzero, FLAG_BOULDER,
	Message, 154,
	Done,

	7,
	SKELETON,
	Present, skeleton,
	Message, 156,
	Done,

	0
};

const byte ePlay[] = {

	14,
	TAMBOURINE,
	At, room17,
	Present, snake,
	Carried, tambourine,
	Zero, FLAG_JINGLE,
	Set, FLAG_SCORE_PLUS,
	Set, FLAG_JINGLE,

	16,
	TAMBOURINE,
	At, room17,
	Present, snake,
	Drop, tambourine,
	Get, tambourine,
	Destroy, snake,
	Message, 157,
	Anykey,
	Desc,

	9,
	TAMBOURINE,
	Drop, tambourine,
	Get, tambourine,
	Message, 158,
	Done,

	11,
	BONE,
	Present, bone,
	Drop, bone,
	Get, bone,
	Message, 159,
	Done,

	0
};

const byte eNum1[] = {

	17,
	ANY,
	Atgt, room29,
	Notzero, FLAG_CARD4,
	Clear, FLAG_CARD4,
	Let, FLAG_PLAY, 1,
	Minus, FLAG_NCARDS, 1,
	Message, 62,
	Done,

	9,
	ANY,
	Atgt, room29,
	Zero, FLAG_CARD4,
	Message, 160,
	Done,

	0
};

const byte eNum2[] = {

	17,
	ANY,
	Atgt, room29,
	Notzero, FLAG_CARD2,
	Clear, FLAG_CARD2,
	Let, FLAG_PLAY, 2,
	Minus, FLAG_NCARDS, 1,
	Message, 63,
	Done,

	9,
	ANY,
	Atgt, room29,
	Zero, FLAG_CARD2,
	Message, 160,
	Done,

	0
};

const byte eNum3[] = {

	17,
	ANY,
	Atgt, room29,
	Notzero, FLAG_CARD1,
	Clear, FLAG_CARD1,
	Let, FLAG_PLAY, 3,
	Minus, FLAG_NCARDS, 1,
	Message, 64,
	Done,

	9,
	ANY,
	Atgt, room29,
	Zero, FLAG_CARD1,
	Message, 160,
	Done,

	0
};

const byte eNum4[] = {

	17,
	ANY,
	Atgt, room29,
	Notzero, FLAG_CARD3,
	Clear, FLAG_CARD3,
	Let, FLAG_PLAY, 4,
	Minus, FLAG_NCARDS, 1,
	Message, 65,
	Done,

	9,
	ANY,
	Atgt, room29,
	Zero, FLAG_CARD3,
	Message, 160,
	Done,

	0
};

const byte eBlow[] = {

	23,
	BONE,
	At, room24,
	Present, boulder,
	Drop, bone,
	Get, bone,
	Destroy, boulder,
	Swap, ant, boulder_1,
	Set, FLAG_BOULDER,
	Message, 161,
	Message, 162,
	Anykey,
	Desc,

	13,
	BONE,
	Present, ant,
	Drop, bone,
	Get, bone,
	Message, 161,
	Message, 163,
	Done,

	9,
	BONE,
	Drop, bone,
	Get, bone,
	Message, 161,
	Done,

	5,
	ANY,
	Message, 164,
	Done,

	0
};

const byte eDig[] = {

	10,
	ANY,
	Present, hole,
	Absent, cart,
	Swap, hole, hole_1,
	Desc,

	7,
	ANY,
	Present, skeleton,
	Message, 165,
	Done,

	5,
	ANY,
	Message, 166,
	Done,

	0
};

const byte eDance[] = {

	7,
	ANY,
	Present, elves,
	Message, 36,
	Done,

	5,
	ANY,
	Message, 167,
	Done,

	0
};

const byte eSwim[] = {

	7,
	ANY,
	At, room14,
	Message, 168,
	Done,

	7,
	ANY,
	At, room24,
	Message, 168,
	Done,

	0
};

const byte eListen[] = {

	7,
	DUNG,
	Present, dung_fly,
	Message, 52,
	Done,

	10,
	ANY,
	At, room9,
	Zero, FLAG_LISTEN,
	Set, FLAG_LISTEN,
	Set, FLAG_SCORE_PLUS,

	7,
	ANY,
	At, room9,
	Message, 169,
	Done,

	7,
	ANY,
	Present, hole,
	Message, 170,
	Done,

	7,
	ANY,
	Present, hole_1,
	Message, 171,
	Done,

	5,
	ANY,
	Message, 172,
	Done,

	0
};

const byte eSmell[] = {

	7,
	DUNG,
	Present, dung,
	Message, 173,
	Done,

	7,
	DUNG,
	Present, dung_fly,
	Message, 173,
	Done,

	7,
	ANY,
	At, room6,
	Message, 174,
	Done,

	7,
	ANY,
	Present, campfire,
	Message, 175,
	Done,

	5,
	ANY,
	Message, 176,
	Done,

	0
};

const byte eSleep[] = {

	5,
	ANY,
	Message, 177,
	Done,

	0
};

const byte eWait[] = {

	5,
	ANY,
	Message, 178,
	Done,

	0
};

const byte eAsk[] = {

	5,
	ANY,
	Message, 179,
	Done,

	0
};

const byte eUse[] = {

	5,
	ANY,
	Message, 180,
	Done,

	0
};

const byte eYes[] = {

	20,
	ANY,
	Notzero, FLAG_YESNO,
	At, room11,
	Present, kitten,
	Destroy, kitten,
	Swap, card0, chick,
	Swap, tree, tree_1,
	Message, 181,
	Anykey,
	Desc,

	19,
	ANY,
	Notzero, FLAG_YESNO,
	At, room8,
	Present, seguin_upseting,
	Place, door_locked, room12,
	Place, salt_stone, room12,
	Clear, FLAG_LICK,
	Goto, room12,
	Desc,

	27,
	ANY,
	Notzero, FLAG_YESNO,
	At, room18,
	Dropall,
	Get, deck,
	Goto, room20,
	Set, FLAG_DARK,
	Clear, FLAG_SHORTCUT,
	Clear, FLAG_BOULDER,
	Clear, FLAG_PATH,
	Clear, FLAG_NOBODY,
	Clear, FLAG_CAMPFIRE,
	Message, 182,
	Anykey,
	Desc,

	0
};

const byte eHelp[] = {

	7,
	ANY,
	Cls,
	Message, 183,
	Anykey,
	Desc,

	0
};

const byte eAbout[] = {

	5,
	ANY,
	Message, 184,
	Done,

	0
};

const byte eVerbs[] = {

	5,
	ANY,
	Message, 185,
	Done,

	0
};

const byte eI[] = {

	7,
	ANY,
	Zero, FLAG_INV,
	Let, FLAG_INV, 1,

	3,
	ANY,
	Inven,

	0
};

const byte eL[] = {

	3,
	ANY,
	Desc,

	0
};

const byte eScore[] = {

	5,
	ANY,
	Score,
	Turns,
	Done,

	0
};

const byte eQ[] = {

	5,
	ANY,
	Quit,
	Turns,
	End,

	0
};

const byte eSave[] = {

	7,
	ANY,
	Atgt, room28,
	Message, 186,
	Done,

	3,
	ANY,
	Save,

	0
};

const byte eLoad[] = {

	3,
	ANY,
	Load,

	0
};

const byte *Event[] = {
	eNone,
	eAny,
	eStatus,
	eN,
	eS,
	eE,
	eW,
	eU,
	eEnter,
	eRun,
	eX,
	eRead,
	eGet,
	eDrop,
	eFill,
	eEmpty,
	eWear,
	eGive,
	ePut,
	eCart,
	eTalk,
	eWake,
	eEat,
	eLick,
	eDrink,
	eOpen,
	eClose,
	eLock,
	eUnlock,
	eRub,
	eBurn,
	eExtinguish,
	eBreak,
	eAttack,
	eMove,
	ePlay,
	eNum1,
	eNum2,
	eNum3,
	eNum4,
	eBlow,
	eDig,
	eDance,
	eSwim,
	eListen,
	eSmell,
	eSleep,
	eWait,
	eAsk,
	eUse,
	eYes,
	eHelp,
	eAbout,
	eVerbs,
	eI,
	eL,
	eScore,
	eQ,
	eSave,
	eLoad,
};
