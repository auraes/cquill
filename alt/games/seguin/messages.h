const wchar_t *message[] = { /* 0-254 */
/* 0 */
	L"\nSeguin: Hi Blanquette, my pretty young goat, come and see me in the stable when you have found and eaten a vegetable, a flower and a fruit that will make your milk fat and fragrant.",
/* 1 */
	L"\n[Type HELP if this is your first time playing this game.]",
/* 2 */
	L"\nSeguin: Hey! Blanquette, come back to me when you have eaten a vegetable, a flower and a fruit.",
/* 3 */
	L"Blanquette climbs onto the barrel, and jumps out of the window.",
/* 4 */
	L"A violent storm suddenly breaks out, pouring a torrential rain that makes the river overflow, washing away part of the bridge that crosses it.\n\nThe party is over, the elves have gone home.",
/* 5 */
	L"\nHermit: I've got everything I need. See you later.",
/* 6 */
	L"\nIt's your turn Blanquette, choose a card from 1 to 4 in your deck.",
/* 7 */
	L"\nThe mongoose fends off Kitten's attack.",
/* 8 */
	L"\nNo one won the round.",
/* 9 */
	L"\nThe mongoose is poisoned by the mushroom.",
/* 10 */
	L"\nBlanquette won the round.",
/* 11 */
	L"\nA 100 t hammer comes out of the joker card and hits the wolf.",
/* 12 */
	L"\nThe snake is eaten by the mongoose.",
/* 13 */
	L"\nBlanquette lost the round.",
/* 14 */
	L"\nThe bird of prey grabs and carries off Kitten.",
/* 15 */
	L"\nThe bird of prey ignores the mushroom.",
/* 16 */
	L"\nThe snake is eaten by the bird of prey!",
/* 17 */
	L"\nBlanquette lose the round.",
/* 18 */
	L"\nThe joker dodges the attack.",
/* 19 */
	L"\nThe kitten lacerates the ectoplasm with its claws.",
/* 20 */
	L"\nThe ectoplasm feasts on the mushroom.",
/* 21 */
	L"\nThe ectoplasm dodges the snake's attack.",
/* 22 */
	L"\n[The score has gone up by 10 points.]",
/* 23 */
	L"\nType X DECK to display the contents of your deck.",
/* 24 */
	L"Kitten: Too bad! See you later.",
/* 25 */
	L"Seguin: Get out of my vegetable garden, then!",
/* 26 */
	L"I can't, since the rope is holding me back.",
/* 27 */
	L"I can't, since the door is closed.",
/* 28 */
	L"I can't, since the boulder is in the way.",
/* 29 */
	L"As Blanquette begins to ascend the mountain, a huge boulder comes loose, crushing everything in its path.",
/* 30 */
	L"The path is too dangerous to ascend it without an experienced guide.",
/* 31 */
	L"I can't, since the door is locked.",
/* 32 */
	L"You can't run away! You have to fight.",
/* 33 */
	L"I can't, since the fence is in the way.",
/* 34 */
	L"I can't reach the window from here.",
/* 35 */
	L"Elf: Hey! Blanquette, where are you going? Comes to dance with us./n",
/* 36 */
	L"Blanquette is dragged into the farandole, and comes out dazed.",
/* 37 */
	L"I can't, since part of the bridge has been washed away by the river.",
/* 38 */
	L"I found a shortcut through a winding path covered in brush!",
/* 39 */
	L"I found a shortcut through a narrow, low passage.",
/* 40 */
	L"I must wake Kitten first.",
/* 41 */
	L"Blanquette jumps on the barrel, peers out of the window on the opposite wall, then climbs back down.",
/* 42 */
	L"I don't jump as far as the springbok.",
/* 43 */
	L"Blanquette gives a kick.",
/* 44 */
	L"I can't, since the hole is under the overturned cart.",
/* 45 */
	L"I can't, since the hole is too narrow.",
/* 46 */
	L"Underground voice: Did you do everything you intended to do around here? (Y/N)",
/* 47 */
	L"The key is at the bottom of the basin, among the dead leaves.",
/* 48 */
	L"A large antique key weakened by rust.",
/* 49 */
	L"All that's left is to make an identical key.",
/* 50 */
	L"A circular percussion instrument with pairs of small metal jingles.",
/* 51 */
	L"A high-quality flytrap.",
/* 52 */
	L"There's a lot of buzz around it!",
/* 53 */
	L"The burlap sack is used as a blanket by the owl.",
/* 54 */
	L"The bag is empty, and just needs to be filled.",
/* 55 */
	L"The bag is full, and ready to go.",
/* 56 */
	L"A flaming torch to see in the dark.",
/* 57 */
	L"It's just waiting to be lighted.",
/* 58 */
	L"It says: Great Kitchen Dictionary by Alexandre Dumas.",
/* 59 */
	L"A pierced bone from a goat's leg.",
/* 60 */
	L"It seems suitable for firewood.",
/* 61 */
	L"The deck contains:\n Basic rules",
/* 62 */
	L" [1] The Kitten card",
/* 63 */
	L" [2] The Toadstool card",
/* 64 */
	L" [3] The Four-leaf Clover card",
/* 65 */
	L" [4] The Venomous Snake card",
/* 66 */
	L" [5] The Morning Star card",
/* 67 */
	L"The card seems cursed, and I have no way of blessing it.",
/* 68 */
	L"With this card added to your deck, the final battle will be in your favour.",
/* 69 */
	L"With this card added to your deck, you're ready for the final battle.",
/* 70 */
	L"You can only usefully examine objects surrounded by (*).\nType HELP, please.",
/* 71 */
	L"It says: When Blanquette has to fight her destiny, choose an available card from your deck, and type the number associated with it.",
/* 72 */
	L"You're obedient, which is good.",
/* 73 */
	L"It's says: Risk of falling rocks!",
/* 74 */
	L"There's a recipe for lamb cooked over a wood fire.",
/* 75 */
	L"It says: There must be another entrance somewhere.",
/* 76 */
	L"I'd rather not, as it could have a negative effect on my deck.",
/* 77 */
	L"I can't reach it from here.",
/* 78 */
	L"\nSalamander: I'll get it for you if you bring me some flies to nibble on.",
/* 79 */
	L"I can leave it there, since I don't need it anymore.",
/* 80 */
	L"Elf: You can have it when the party's over.",
/* 81 */
	L"Hermit: Hey! Nobody, you know well that I hate anybody touching my stuff.",
/* 82 */
	L"I can't, since the sign is stuck under the boulder.",
/* 83 */
	L"You can only get objects surrounded by (*).\nType HELP, please.",
/* 84 */
	L"You can't, since Blanquette need it for the final fight.",
/* 85 */
	L"The bag is already full of flour.",
/* 86 */
	L"You fill the bag with the flour from the millstone.",
/* 87 */
	L"There isn't anything obvious with which to fill it.",
/* 88 */
	L"The bag is already empty.",
/* 89 */
	L"I'd rather not. I'll never again be restrained by a collar and leash, rope or chain.",
/* 90 */
	L"The flies rush to the salamander, which swallows them at once. The salamander vanishes under the leaves after having brought back the key.",
/* 91 */
	L"Mule: Thanks! This will make a pretty bell collar. See you later.",
/* 92 */
	L"Snake: I'm not a pet to be tied up or caged!",
/* 93 */
	L"But the bag is empty at the moment.",
/* 94 */
	L"Mule: PUT it in the cart.",
/* 95 */
	L"Hermit: Thanks! Nobody. And what else?",
/* 96 */
	L"No one seems interested.",
/* 97 */
	L"In what?",
/* 98 */
	L"Mule: Thanks! See you later.",
/* 99 */
	L"Kitten: Hi Blanquette, I can't see my beloved sister from up here. Do you know where she's hidden? (Y/N)",
/* 100 */
	L"Seguin: Congratulations Blanquette, your milk is of very good quality. Now that my bucket is full, I'm going to take a little nap.",
/* 101 */
	L"Seguin: A rabbit stole my carrots again! Hey! Blanquette, what's going on?\n\n*: I want to go to the mountain, Mr Seguin.\n\nSeguin: But don't you know that there's a wolf in the mountain? What will you do when he comes?\n\n*: It doesn't matter, Mr Seguin, let me go to the mountain.\n\nSeguin: I will lock you in the stable, and you will stay there!\n",
/* 102 */
	L"Seguin: Did you do everything you intended to do around here? (Y/N)",
/* 103 */
	L"Rooster: Hi Blanquette, the wild freedom is on the other side of the wall. LISTEN to the mountain breath, and you will know the great price of freedom.",
/* 104 */
	L"\n*: How good it must be up there! Without fences or rope that skin my neck.",
/* 105 */
	L"\nRooster: By the way, I saw Mr Seguin in the vegetable garden.",
/* 106 */
	L"Rooster: COCK-A-DOODLE-DO!",
/* 107 */
	L"Chick: Hi Blanquette, if you see my brother Kitten, don't tell him where I'm hiding!",
/* 108 */
	L"Chick: CHEEP-CHEEP!",
/* 109 */
	L"Salamander: Hi Blanquette, you shouldn't be here, Mr Seguin will be worried.",
/* 110 */
	L"Elf: Hi Blanquette, we're celebrating the Shepherd's day, come to dance with us./n",
/* 111 */
	L"Mule: Hi Blanquette, I have to fetch the wrecked hay wagon, but a nasty snake is in my way. Oh, if only I had my bell collar to scare him off!",
/* 112 */
	L"Mule: Hi Blanquette, I have to bring a bag of flour to Mr Seguin. If you have one, PUT it in the cart.",
/* 113 */
	L"Snake: Hi blanquette, the mule, far too clumsy to take the shortcut, nearly ran me over as I passed. Next time, I'll bite her!",
/* 114 */
	L"Owl: Hi blanquette, I see little rabbits grazing on the clover outside the windmill, but I'm too old to chase them.",
/* 115 */
	L"The scared rabbit runs inside windmill at high speed.",
/* 116 */
	L"The scared rabbit runs away at high speed.",
/* 117 */
	L"The frightened rabbit runs away at high speed, and hits the closed door.\n\nOwl: Hey! what's going on?\n\nThe owl dives on the stunned rabbit, and carries its prey to safety.\n\nThe burlap bag fell off the roof framework.",
/* 118 */
	L"Hermit: Hi Nobody, bring me some stale bread, wood and paper so I can make a campfire toast.",
/* 119 */
	L"\n*: I'm not Nobody, I'm Blanquette!\n\nHermit: Ha ha ha! And I'm Polypheme! What a joker this Nobody.",
/* 120 */
	L"Ant: Hi Blanquette, the hermit and his lamb trample us every time they come out of the cave. If you find anything to block the entrance, let me know where by blowing a loud whistle.",
/* 121 */
	L"Robin: Hi Blanquette, I'm sure the hermit wants to eat me to atone for his sins!",
/* 122 */
	L"Lamb: BAA MAA!",
/* 123 */
	L"Chamois: It's time to face your destiny, Blanquette.\n\nThe deck of cards shakes and lights up, the Morning Star card springs up to join the firmament. It's pitch-dark now.",
/* 124 */
	L"Chamois: Hi Blanquette, find the Morning Star card, and I'll lead you on the path to your destiny.\nDon't forget to SAVE before the final battle.",
/* 125 */
	L"Moth: ...",
/* 126 */
	L"Moth: Hi Blanquette, I saw a lamb runs away at high speed to the west, south and west again.",
/* 127 */
	L"There's no reply.",
/* 128 */
	L"Kitten stretches, licks his paw, and disgusted by the taste of salt, runs away through the window.",
/* 129 */
	L"Kitten stretches, licks his paw to clean his hair, and falls back to sleep.",
/* 130 */
	L"Nothing can wake him up.",
/* 131 */
	L"It's not for eating, it's for licking.",
/* 132 */
	L"It's not my favourite meal.",
/* 133 */
	L"My tongue is now full of salt.",
/* 134 */
	L"Kitten purrs with pleasure in his sleep.",
/* 135 */
	L"Blanquette quenches its thirst, and splashes with fresh water.",
/* 136 */
	L"The lamb seems hypnotized by the water's reflection of Blanquette.\n\nRobin: Hey! Blanquette, what big eyes you have! What big ears you have! What big teeth you have!\n\nThe scared lamb runs away at high speed.",
/* 137 */
	L"There's nothing suitable to drink here.",
/* 138 */
	L"The door is already open.",
/* 139 */
	L"The door is already closed.",
/* 140 */
	L"Seguin: Hey! Blanquette, leave this door open.",
/* 141 */
	L"The door is already locked.",
/* 142 */
	L"I can't, since the door is opened.",
/* 143 */
	L"I need a key.",
/* 144 */
	L"I achieve nothing by this.",
/* 145 */
	L"There's nothing here to light it.",
/* 146 */
	L"Renaude didn't want to be cremated.",
/* 147 */
	L"This dangerous act would achieve little.",
/* 148 */
	L"The flame dies, and is reborn again.",
/* 149 */
	L"I can't, since the rope is strong and the knot very tight.",
/* 150 */
	L"I could get hurt.",
/* 151 */
	L"Use your cards to fight.",
/* 152 */
	L"Violence isn't the answer to this one.",
/* 153 */
	L"I don't have enough strength to lift it.",
/* 154 */
	L"I don't have enough strength to move it.",
/* 155 */
	L"The barrel rolls under the window.",
/* 156 */
	L"Leave her in peace. The vultures will take care of it.",
/* 157 */
	L"~TING-A-LING!~\n\nAt the sound of the jingle, the snake runs away at high speed.",
/* 158 */
	L"~TING-A-LING!~",
/* 159 */
	L"Try to BLOW into it instead.",
/* 160 */
	L"This card is not in your deck.",
/* 161 */
	L"~TWEET TWEET TWEET!~",
/* 162 */
	L"\nThe ground darkened and began to move, dragging the rock with it.\n\nAnt: Thanks! See you later.",
/* 163 */
	L"\nAnt: Blow a loud whistle where you find anything to block the entrance.",
/* 164 */
	L"~TWEET!~",
/* 165 */
	L"Renaude didn't want to be buried.",
/* 166 */
	L"Digging would achieve nothing here.",
/* 167 */
	L"Blanquette move in a graceful and rhythmical way.",
/* 168 */
	L"The river's current is too strong.",
/* 169 */
	L"I hear a wolf howling in the distance.",
/* 170 */
	L"Underground voice: BAA MAA!",
/* 171 */
	L"~BAA MAA!~",
/* 172 */
	L"I hear nothing unexpected.",
/* 173 */
	L"It doesn't smell like roses.",
/* 174 */
	L"A subtle flowery fragrance.",
/* 175 */
	L"Robin's body odour.",
/* 176 */
	L"I smell nothing unexpected.",
/* 177 */
	L"I'm not feeling especially drowsy.",
/* 178 */
	L"Nothing happens.",
/* 179 */
	L"Use TALK to communicate with characters.",
/* 180 */
	L"You don't need to use this verb in this game.",
/* 181 */
	L"Kitten: Thanks! See you later.",
/* 182 */
	L"Blanquette is sucked down the rabbit hole, and falls back into a cave, onto a lamb that cushions her fall.",
/* 183 */
	L"The game expect commands in the form of VERB NOUN (not case-sensitive).\n\nBasic commands:\nL or R to refresh the location display.\nN, S, E, W, U & D to move around.\nI to display what you're carrying and wearing.\nX to examine an object.\nGET & DROP to pick up or drop an object.\nTALK to communicate with a character.\nVERBS to display commands needed to complete the game.\nSAVE & RESTORE to save or restore the game.\n\nInteract only with objects displayed after: I notice.\nEXAMINE (X) and GET are only useful for objects surrounded by (*) e.g. A *key*.",
/* 184 */
	L"Mr Seguin's Goat (c) 2023 Lionel Ange.\nBased on a short story in Letters from My Windmill (1869), by the French writer and playwright Alphonse Daudet.\nPlaytesting by Nobody and Polypheme.\n\nRelease 1.2 / Serial number 052424 / cQuill\nThe game code is compatible with The Quill (Gilsoft).\n\nThis game was created for Text Adventure Literacy Jam in May 2023.",
/* 185 */
	L"Some useful commands:\nL (look), I (inventory)\nX (examine), READ\nN, S, E, W, U, D, CLIMB, JUMP, GO\nTALK, GIVE, WAKE, EAT, DRINK, LICK, BLOW\nGET, DROP, PUT, WEAR, REMOVE, FILL, EMPTY\nOPEN, CLOSE, UNLOCK, MOVE, DIG\nLIGHT, EXTINGUISH, CLEAN, PLAY, LISTEN\nSAVE, RESTORE, YES, NO, SCORE\nHELP, ABOUT, VERBS\n\nAnd other commands (or synonyms) that you will discover by yourself.",
/* 186 */
	L"It's too late to save. Blanquette must face her destiny.",
};
