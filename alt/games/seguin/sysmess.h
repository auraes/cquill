const wchar_t *sysmess[] = {
/* 0 */
    L"It's pitch dark here! I can't see a thing.\n\n A moth swirls around Blanquette\n",
/* 1 */
    L"\nI notice:\n",
/* 2 */
    L"\n",
/* 3 */
    L"\n",
/* 4 */
    L"\n",
/* 5 */
    L"\n",
/* 6 */
    L"Sorry, I don't understand that. Try some different words.\n",
/* 7 */
    L"I can't go that way.\n",
/* 8 */
    L"I can't.\n",
/* 9 */
    L"You're carrying:\n",
/* 10 */
    L" (worn)",
/* 11 */
    L" Nothing at all.\n",
/* 12 */
    L"Do you really want to quit now?\n",
/* 13 */
    L"\nEND OF GAME\n\nThanks for playing. Do you want to try again?\n",
/* 14 */
    L"Bye. Have a nice day.\n",
/* 15 */
    L"OK.\n",
/* 16 */
    L"Press Enter to continue",
/* 17 */
    L"You have taken ",
/* 18 */
    L" turn",
/* 19 */
    L"s",
/* 20 */
    L".\n",
/* 21 */
    L"You have scored ",
/* 22 */
    L"%\n",
/* 23 */
    L"I'm not wearing it.\n",
/* 24 */
    L"You can't. Your hands are full.\n",
/* 25 */
    L"You already have it.\n",
/* 26 */
    L"It's not here.\n",
/* 27 */
    L"You can't carry any more.\n",
/* 28 */
    L"You don't have it.\n",
/* 29 */
    L"I'm already wearing it.\n",
/* 30 */
    L"Y\n",
/* 31 */
    L"N\n",
/* 32 */
   L"Save failed.\n",
/* 33 */
   L"Type in filename.\n",
/* 34 */
   L"Restore failed.\n",
};
