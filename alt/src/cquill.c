/*
 *
 * Auraes, 2021-2025
 * Licence Creative Commons BY-SA 4.0 (CC BY-SA 4.0)
 * https://creativecommons.org/licenses/by-sa/4.0/deed.fr
 *
 * The Quill is copyright Gilsoft and its creators 1983-2023.
 * UnQuill is copyright 1996-2002, 2011, 2012, 2015, 2023 by John Elliott.
 *
 */

/*
gcc -Wall -W -O3 -std=c99 src/cquill.c -Igames/lotg -olotg
gcc -Wall -W -O3 -std=c99 src/cquill.c -Igames/seguin -oseguin
*/
/*
#ifndef _XOPEN_SOURCE_EXTENDED
#define _XOPEN_SOURCE_EXTENDED 1
#endif
*/
#include <wchar.h>
#include <wctype.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

#include "cquill.h"
#include "vocabs.h"
#include "rooms.h"
#include "items.h"
#include "sysmess.h"
#include "messages.h"
#include "process.h"

FILE *fp;
const byte *ip;
byte flag[FLAGMAX+ITEMMAX];
byte ram[1+FLAGMAX+ITEMMAX];
byte *at = flag+FLAGMAX;
wchar_t buffer[INPUTMAX+1];
int done, isdone;
bool diagnostic;

#include "condacts.c"

#ifdef WORDWRAP
int curx, col;

void get_win_value(void)
{
    struct winsize win;

    if (ioctl(STDIN_FILENO, TIOCGWINSZ, &win) == -1)
        col = 80;
    else
        col = win.ws_col;
}

void newline(void)
{
    if (curx < col)
        putch('\n');
    curx = 0;
}

void print_c(const wchar_t c)
{
    if (c == L'\n')
        newline();
    else {
        putch(c);
        ++curx;
    }
}

const wchar_t *line(const wchar_t *s)
{
    const wchar_t *p = NULL;
    int x = curx;

    while (x <= col) {
        if (*s == L'\0' || *s == L'\n')
            return s;
        if (*s == L' ')
            p = s;
        ++s;
        ++x;
    }
    if (p == NULL) {
        while (*s && *s != L' ' && *s != L'\n') {
            ++s;
            ++x;
        }
        if ((x-curx) <= col) //FIXME <, <=
            newline();
        p = s;
    }
    return p;
}

void cputs(const wchar_t *s)
{
    const wchar_t *p;

    while (1) {
        p = line(s);
        while (s < p)
            print_c(*s++);
        if (*s == L'\0')
            break;
        newline();
        ++s;
    }
}
#endif

char *filename(void)
{
    static char savefile[8+1];

    putsysm(33);
    lineinput();
    if (*buffer) {
        *(buffer+8) = L'\0';
        if (wcstombs(savefile, buffer, 8) > 0)
            return savefile;
    }
    return NULL;
}

bool loadgame(char *s)
{
    size_t nblocs;

    fp = fopen(s, "rb");
    if (fp) {
        nblocs = fread(flag, 1, NBLOCS, fp);
        fclose(fp);
        if (nblocs == NBLOCS)
            return true;
    }
    return false;
}

bool savegame(char *s)
{
    size_t nblocs;

    fp = fopen(s, "wb");
    if (fp) {
        nblocs = fwrite(flag, 1, NBLOCS, fp);
        fclose(fp);
        if (nblocs == NBLOCS)
            return true;
    }
    return false;
}

/* "Do you require diagnostics?" */
void diagnostics(void)
{
    int i;

    newline();
    for (i=1; i<=36; i++) {
        wprintf(L"%3d ", flag[i-1]);
        if (!(i % 10))
            newline();
    }
    newline();
}

void putnum(word n)
{
    wchar_t *p; /* WARNING buffer */

    p = buffer + 5;
    *p = L'\0';
    do {
        --p;
        *p = n % 10 + L'0';
    } while (n /= 10);
    cputs(p);
}

void listitems(void)
{
    int i;
    bool flg;

    flg = true;
    for (i=0; i<ITEMMAX; i++) {
        if (at[i] == location) {
            if (flg) {
                flg = false;
                putsysm(1);
            }
            cputs(Item[i].describe);
            newline();
        }
    }
}

bool compass(void)
{
    const byte *p;

    p = Room[location].exit;
    if (p) {
        while (*p) {
            if (*p == w1) {
                location = *(p+1);
                return true;
            }
            p += 2;
        }
    }
    return false;
}

int wordvalue(const wchar_t *s)
{
    int i = 0;

    if (*s >= L'a' && *s <= L'z')
        i = index_letter[*s - L'a'];
#ifdef DIGITS
    else if (*s >= L'0' && *s <= L'9')
        i = index_digit[*s - L'0'];
#endif
    while (*Vocab[i].tag == *s) {
        if (wcsncmp(Vocab[i].tag, s, TRUNCATE) == 0)
            return i;
        --i;
    }
    return ANY;
}

bool yesno(int n)
{
    lineinput();
    if (*buffer == (wchar_t) towlower(*sysmess[n]))
        return true;
    return false;
}

void lineinput(void)
{
    wchar_t *p;
    wint_t c;

    prompt(L'>');
    p = buffer;
    if (cgets(p, INPUTMAX+1)) {
        while (*p != L'\0') {
            if (*p == L'\n') {
                *p = L'\0';
                return;
            }
            *p = towlower(*p);
            ++p;
        }
        do {
            c = getch();
        } while (c != L'\n' && c != WEOF);
    }
}

void getinput(void)
{
    wchar_t *p, *ps;
    byte wn;

    lineinput();
    w1 = ANY;
    evt = 0;
    p = wcstok(buffer, L" ", &ps);
    while (p) {
        wn = wordvalue(p);
        if (Vocab[wn].type & FLAG_SYNO)
            wn = Vocab[wn].value;
        if (wn < ANY) {
            if (w1 == ANY) {
                w1 = wn;
                w2 = ANY;
                if (Vocab[wn].type & FLAG_EVEN)
                    evt = Vocab[wn].value;
            }
            else {
                w2 = wn;
                break;
            }
        }
        p = wcstok(NULL, L" ", &ps);
    }
}

void process(const byte *p)
{
    const byte *ips;

    done = isdone = NOTDONE;
    ip = ips = p;
    while (*ip) {
        ips += *ip;
        ++ip;
        if (p == eStatus)
            goto duck;
        if ((p == eAny) || (*ip == ANY || *ip == w2)) {
            ++ip;
            duck:
            while (ip < ips) {
                if ((*ip) & 0x80u) {
                    isdone = DONE;
                    (action[(*ip) & 0x7fu])();
                    if (done)
                        return;
                }
                else if (!(condition[*ip])())
                    break;
                ++ip;
            }
        }
        ip = ips;
    }
}

void gameloop(void)
{
    int i;

    i = FLAGMAX;
    while (i)
        flag[--i] = 0;
    *ram = 0;
    i = ITEMMAX;
    while (i) {
        --i;
        at[i] = Item[i].initial;
        if (at[i] == CARRIED)
            ++count;
    }

    redescribe:
    cls_();
    if (flag2)
        --flag2;
    if (light && flag3)
        --flag3;
    if (light && !olight() && flag4)
        --flag4;
    if (!light || olight()) {
        cputs(Room[location].describe);
        newline();
        listitems();
    }
    else
        putsysm(0);

    while (true) {
        process(eStatus);
        if (done == DESC)
            goto redescribe;
        if (done == END)
            break;

        if (flag5)
            --flag5;
        if (flag6)
            --flag6;
        if (flag7)
            --flag7;
        if (flag8)
            --flag8;
        if (light && flag9)
            --flag9;
        if (light && !olight() && flag10)
            --flag10;

        ++tlsb;
        if (!tlsb)
            ++tmsb;
        if (diagnostic)
            diagnostics();
        putsysm((rand() % 4) + 2);
        getinput();

        if (w1 < ANY) {
            if (compass())
                goto redescribe;
            process(Event[evt]);
            if (done == NOTDONE)
                process(eAny);
            if (done == DESC)
                goto redescribe;
            if (done == END)
                break;
            if (isdone == NOTDONE) {
                if (Vocab[w1].type & FLAG_COMP)
                    putsysm(7);
                else
                    putsysm(8);
            }
        }
        else
            putsysm(6);
    }
}

int main(int argc, char *argv[])
{
    if (argc > 1 && strcmp(argv[1], "-d") == 0)
        diagnostic = true;
    setlocale(LC_ALL, "");
#ifdef WORDWRAP
    get_win_value();
#endif
    srand(time(NULL));
    do {
        gameloop();
    } while (!yesno(31));
    putsysm(14);

    return EXIT_SUCCESS;
}
