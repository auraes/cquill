int oid(void)
{
   if (w2 && w2 < ANY && (Vocab[w2].type & FLAG_ITEM))
      return Vocab[w2].value;
   return ITEMMAX;
}

void oget(byte id)
{
   if (at[id] == CARRIED || at[id] == WORN) {
      putsysm(25);
      done = DONE;
   }
   else if (at[id] != location) {
      putsysm(26);
      done = DONE;
   }
   else if (count == CAPACITY) {
      putsysm(27);
      done = DONE;
   }
   else {
      at[id] = CARRIED;
      ++count;
   }
}

void odrop(byte id)
{
   if (at[id] == WORN && count == CAPACITY) {
      putsysm(24);
      done = DONE;
   }
   else if (at[id] != CARRIED && at[id] != WORN) {
      putsysm(28);
      done = DONE;
   }
   else {
      if (at[id] == CARRIED)
         --count;
      at[id] = location;
   }
}

void owear(byte id)
{
   if (at[id] == WORN) {
      putsysm(29);
      done = DONE;
   }
   else if (at[id] != CARRIED) {
      putsysm(28);
      done = DONE;
   }
   else {
      at[id] = WORN;
      --count;
   }
}

void oremove(byte id) //disrobe
{
   if (at[id] != WORN) {
      putsysm(23);
      done = DONE;
   }
   else if (count == CAPACITY) {
      putsysm(24);
      done = DONE;
   }
   else {
      at[id] = CARRIED;
      ++count;
   }
}

bool olight(void)
{
   if (at[0] == location || at[0] == CARRIED || at[0] == WORN)
      return true;
   return false;
}

/* common conditions */
bool at_(void) /* locno */
{
   ++ip;
   if (location == *ip)
      return true;
   return false;
}

bool notat_(void) /* locno */
{
   ++ip;
   if (location != *ip)
      return true;
   return false;
}

bool atgt_(void) /* locno */
{
   ++ip;
   if (location > *ip)
      return true;
   return false;
}

bool atlt_(void) /* locno */
{
   ++ip;
   if (location < *ip)
      return true;
   return false;
}

bool present_(void) /* objno */
{
   ++ip;
   if (at[*ip] == location || at[*ip] == CARRIED
      || at[*ip] == WORN)
      return true;
   return false;
}

bool absent_(void) /* objno */
{
   ++ip;
   if (at[*ip] != location && at[*ip] != CARRIED
      && at[*ip] != WORN)
      return true;
   return false;
}

bool worn_(void) /* objno */
{
   ++ip;
   if (at[*ip] == WORN)
      return true;
   return false;
}

bool notworn_(void) /* objno */
{
   ++ip;
   if (at[*ip] != WORN)
      return true;
   return false;
}

bool carried_(void) /* objno */
{
   ++ip;
   if (at[*ip] == CARRIED)
      return true;
   return false;
}

bool notcarr_(void) /* objno */
{
   ++ip;
   if (at[*ip] != CARRIED)
      return true;
   return false;
}

bool chance_(void) /* percent (1-100 inclusive) */
{
   ++ip;
   if ((rand() % 100)+1 <= *ip)
      return true;
   return false;
}

bool zero_(void) /* flagno */
{
   ++ip;
   if (flag[*ip] == 0)
      return true;
   return false;
}

bool notzero_(void) /* flagno */
{
   ++ip;
   if (flag[*ip] != 0)
      return true;
   return false;
}

bool eq_(void) /* flagno value */
{
   ip += 2;
   if (flag[*(ip-1)] == *ip)
      return true;
   return false;
}

bool gt_(void) /* flagno value */
{
   ip += 2;
   if (flag[*(ip-1)] > *ip)
      return true;
   return false;
}

bool lt_(void) /* flagno value */
{
   ip += 2;
   if (flag[*(ip-1)] < *ip)
      return true;
   return false;
}

/* not common conditions */
bool noteq_(void) /* flagno value */
{
   ip += 2;
   if (flag[*(ip-1)] != *ip)
      return true;
   return false;
}

bool created_(void) /* objno */
{
   ++ip;
   if (at[*ip] != NOTCREATED)
      return true;
   return false;
}

bool destroyed_(void) /* objno */
{
   ++ip;
   if (at[*ip] == NOTCREATED)
      return true;
   return false;
}

/* common actions */
void inven_(void)
{
   int i;
   bool flg;

   flg = false;
   putsysm(9);
   for (i=0; i<ITEMMAX; i++) {
      if (at[i] == CARRIED || at[i] == WORN) {
         flg = true;
         cputs(Item[i].describe);
         if (at[i] == WORN)
            putsysm(10);
         newline();
      }
   }
   if (!flg)
      putsysm(11);
   done = DONE;
}

void desc_(void)
{
   done = DESC;
}

void quit_(void)
{
   putsysm(12);
   if (!yesno(30))
      done = DONE;
}

void end_(void)
{
   putsysm(13);
   done = END;
}

void done_(void)
{
   done = DONE;
}

void ok_(void)
{
   putsysm(15);
   done = DONE;
}

void anykey_(void)
{
   newline(); /* FIXME */
   putsysm(16);  //More... (C64)
   getch();
   newline(); /* FIXME */
}

void save_(void)
{
   char *str;

   str = filename();
   if (str && savegame(str))
      done = DESC;
   else
      putsysm(32); /* WARNING: differing original */
}

void load_(void)
{
   char *str;

   str = filename();
   if (str && loadgame(str))
      done = DESC;
   else
      putsysm(34); /* WARNING: differing original */
}


void turns_(void)
{
   word num;

   num = tlsb + (tmsb << 8);
   putsysm(17);
   putnum(num);
   putsysm(18);
   if (num != 1)
      putsysm(19); /* WARNING: French num > 0 */
   putsysm(20);
}

void score_(void)
{
   putsysm(21);
   putnum(score);
   putsysm(22);
}

void pause_(void) /* value */
{
   ++ip;
   if (*ip)
      sleep(*ip/50);
   else
      sleep(5); /* 256/50 */
}

void goto_(void) /* locno */
{
   ++ip;
   location = *ip;
}

void message_(void) /* mesno */
{
   ++ip;
   putmess(*ip);
   newline();
}

void remove_(void) /* objno */
{
   ++ip;
   oremove(*ip);
}

void get_(void) /* objno */
{
   ++ip;
   oget(*ip);
}

void wear_(void) /* objno */
{
   ++ip;
   owear(*ip);
}

void drop_(void) /* objno */
{
   ++ip;
   odrop(*ip);
}

void destroy_(void) /* objno */
{
   ++ip;
   if (at[*ip] == CARRIED)
      --count;
   at[*ip] = NOTCREATED;
}

void create_(void) /* objno */
{
   ++ip;
   if (at[*ip] == CARRIED)
      --count;
   at[*ip] = location;
}

void swap_(void) /* objno objno */
{
   byte sw;

   ip += 2;
   sw = at[*ip];
   at[*ip] = at[*(ip-1)];
   at[*(ip-1)] = sw;
}

void set_(void) /* flagno */
{
   ++ip;
   flag[*ip] = 0xffu;
}

void clear_(void) /* flagno */
{
   ++ip;
   flag[*ip] = 0;
}

void plus_(void)  /* flagno value */
{
   ip += 2;
   if (flag[*(ip-1)] + *ip > 0xffu)
      flag[*(ip-1)] = 0xffu;
   else
      flag[*(ip-1)] += *ip;
}

void minus_(void) /* flagno value */
{
   ip += 2;
   if (flag[*(ip-1)] < *ip)
      flag[*(ip-1)] = 0;
   else
      flag[*(ip-1)] -= *ip;
}

void let_(void) /* flagno value */
{
   ip += 2;
   flag[*(ip-1)] = *ip;
}

/* not common actions */

void dropall_(void)
{
   int i;

   i = ITEMMAX;
   while (i) {
      --i;
      if (at[i] == CARRIED || at[i] == WORN)
         at[i] = location;
   }
   count = 0;
}

void place_(void) /* objno locno (range 0-251) */
{
   ip += 2;
   if (at[*(ip-1)] == CARRIED)
      --count;
   if (*ip == CARRIED)
      ++count;
   at[*(ip-1)] = *ip;
}

void autog_(void)
{
   int id;

   id = oid();
   if (id < ITEMMAX)
      oget(id);
   else {
      putsysm(8);
      done = DONE;
   }
}

void autod_(void)
{
   int id;

   id = oid();
   if (id < ITEMMAX)
      odrop(id);
   else {
      putsysm(8);
      done = DONE;
   }
}

void autow_(void)
{
   int id;

   id = oid();
   if (id < ITEMMAX && Vocab[w2].type & FLAG_CLOT)
      owear(id);
   else {
      putsysm(8);
      done = DONE;
   }
}

void autor_(void)
{
   int id;

   id = oid();
   if (id < ITEMMAX && Vocab[w2].type & FLAG_CLOT)
      oremove(id);
   else {
      putsysm(8);
      done = DONE;
   }
}

void mes_(void) /* mesno */
{
   ++ip;
   putmess(*ip);
}

void star_(void) /* mesno */
{
   ++ip;
}

void sysmess_(void) /* sysno */
{
   ++ip;
   putsysm(*ip);
}

void add_(void)  /* flagno flagno */
{
   ip += 2;
   if (flag[*(ip-1)] + flag[*ip] > 0xffu)
      flag[*(ip-1)] = 0xffu;
   else
      flag[*(ip-1)] += flag[*ip];
}

void sub_(void)  /* flagno flagno */
{
   ip += 2;
   if (flag[*(ip-1)] < flag[*ip])
      flag[*(ip-1)] = 0;
   else
      flag[*(ip-1)] -= flag[*ip];
}

void jsr_(void)  /* lsb msb */
{
   ip += 2;
}

void print_(void) /* flagno */
{
   ++ip;
   putnum(*ip); /* WARNING: flag 31-32 (turns) */
}

void ramsave_(void)
{
   byte *pt;
   int i;

   pt = ram+1;
   i = (FLAGMAX+ITEMMAX);
   while (i)
      *pt++ = flag[--i];
   *ram = 1;
   done = DESC;
}

void ramload_(void)
{
   byte *pt;
   int i;

   if (*ram) {
      pt = ram+1;
      i = (FLAGMAX+ITEMMAX);
      while (i)
         flag[--i] = *pt++;
      done = DESC;
   }
   else putsysm(33);
}

/* machine-specific displays actions */

void border_(void) /* value */
{
   ++ip;
}

void paper_(void) /* value */
{
   ++ip;
}

void ink_(void) /* value */ /* WARNING: CPC value value */
{
   ++ip;
}

void screen_(void) /* value */
{
   ++ip;
}

void text_(void) /* intensity */
{
   ++ip;
}

void cls_(void)
{
    if (cls()) {}; /* pour le WARNING avec -O */
}

/* machine-specific sound actions */

void beep_(void) /* duration pitch */
{
   ip += 2;
}

void sound_(void) /* WARNING: machine-specific arguments */
{
   ip += 2;
}

void sid_(void) /* regno value */
{
   ip += 2;
}

void music_(void) /* note duration */
{
   ip += 2;
}

void volume_(void) /* value */
{
   ++ip;
}
