const char *sysmess[] = { /* 0-34 */
/* 0 */
   "Everything is dark. You can't see.\n",
/* 1 */
   "\nYou can also see:-\n",
/* 2 */
   "\nWhat next?\n",
/* 3 */
   "\nWhat next?\n",
/* 4 */
   "\nWhat next?\n",
/* 5 */
   "\nWhat next?\n",
/* 6 */
   "Sorry, I don't understand you.\n",
/* 7 */
   "You can't go that way.\n",
/* 8 */
   "You can't.\n",
/* 9 */
   "You have with you:-\n",
/* 10 */
   " (worn)",
/* 11 */
   "Nothing at all.\n",
/* 12 */
   "Do you really want to quit now?\n",
/* 13 */
   "\n              END OF GAME\n"
   "\nDo you want to try again?\n",
/* 14 */
   "Bye for now.\n",
/* 15 */
   "OK.\n",
/* 16 */
   "         Press Enter to continue",
/* 17 */
   "You have taken ",
/* 18 */
   " turn",
/* 19 */
   "s",
/* 20 */
   ".\n",
/* 21 */
   "You have scored ",
/* 22 */
   "%\n",
/* 23 */
   "You're not wearing it.\n",
/* 24 */
   "You can't carry any more.\n",
/* 25 */
   "You already have it.\n",
/* 26 */
   "It's not here.\n",
/* 27 */
   "You can't carry any more.\n",
/* 28 */
   "You don't have it.\n",
/* 29 */
   "You're already wearing it.\n",
/* 30 */
   "Y\n",
/* 31 */
   "N\n",
/* 32 */
   "Save failed.\n",
/* 33 */
   "Type in filename.\n",
/* 34 */
   "Restore failed.\n"
};
