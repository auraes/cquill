#define CAPACITY 3

enum {
   mouse,
   key,
   axe,
   mother_1,
   children,
   giant,
   egg,
   bean,
   beanstalk,
   open_door,
   broken_door,
   mother_2,
   mother_3
};

/*
   NOTCREATED
   WORN
   CARRIED
*/

Items Item[] = { /* 0-254 */
/* mouse */
   {
      "* A tiny mouse",
      NONE,
      dining_room
   },
/* key */
   {
      "* A silver key",
      NONE,
      NOTCREATED
   },
/* axe */
   {
      "* An axe",
      NONE,
      garden_shed
   },
/* mother_1 */
   {
      "* Mother is here",
      NONE,
      cottage
   },
/* children */
   {
      "* The Children, Peter and Mary, are here.",
      NONE,
      green_room
   },
/* giant */
   {
      "* The --- G I A N T ---",
      NONE,
      huge_room
   },
/* egg */
   {
      "* The GOLDEN EGG",
      NONE,
      huge_room
   },
/* bean */
   {
      "* A bean",
      NONE,
      cottage
   },
/* beanstalk */
   {
      "* A giant beanstalk",
      NONE,
      NOTCREATED
   },
/* open_door */
   {
      "* An open door",
      NONE,
      NOTCREATED
   },
/* broken_door */
   {
      "* A broken door",
      NONE,
      NOTCREATED
   },
/* mother_2 */
   {
      "* Mother is here holding the GOLDEN EGG.",
      NONE,
      NOTCREATED
   },
/* mother_3 */
   {
      "* Mother is here width THE CHILDREN.",
      NONE,
      NOTCREATED
   }
};
