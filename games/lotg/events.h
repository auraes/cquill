#define FLAG_KEY 20
#define FLAG_BEAN 21

const byte StatusTable[] = {
   9,
   D, ANY,
   At, intro0,
   Anykey,
   Goto, intro2,
   Desc,

   9,
   D, ANY,
   At, intro1,
   Anykey,
   Goto, cottage,
   Desc,

   9,
   D, ANY,
   At, intro2,
   Anykey,
   Goto, intro1,
   Desc,

   0
};

const byte EventTable[] = {
   10,
   N, ANY,
   At, gallery,
   Absent, open_door,
   Message, 1,
   Done,

   10,
   N, ANY,
   At, gallery,
   Present, open_door,
   Goto, huge_room,
   Desc,

   10,
   W, ANY,
   At, huge_room,
   Absent, broken_door,
   Message, 1,
   Done,

   10,
   W, ANY,
   At, huge_room,
   Present, broken_door,
   Goto, green_room,
   Desc,

   8,
   X, BEAN,
   Present, bean,
   Message, 11,
   Done,

   8,
   X, BEAN,
   Present, beanstalk,
   Message, 14,
   Done,

   14,
   X, BENCH,
   At, kitchen,
   Zero, FLAG_KEY,
   Message, 6,
   Create, key,
   Set, FLAG_KEY,
   Done,

   10,
   X, BENCH,
   At, kitchen,
   Notzero, FLAG_KEY,
   Message, 0,
   Done,

   8,
   X, KEY,
   Present, key,
   Message, 27,
   Done,

   8,
   X, MOUSE,
   Present, mouse,
   Message, 24,
   Done,

   8,
   X, EGG,
   Present, egg,
   Message, 13,
   Done,

   10,
   X, GARDEN,
   At, garden,
   Absent, beanstalk,
   Message, 30,
   Done,

   10,
   X, GARDEN,
   At, garden,
   Present, beanstalk,
   Message, 31,
   Done,

   8,
   X, SHED,
   At, garden,
   Message, 32,
   Done,

   8,
   X, MOTHER,
   Present, mother_1,
   Message, 28,
   Done,

   8,
   X, MOTHER,
   Present, mother_2,
   Message, 28,
   Done,

   8,
   X, MOTHER,
   Present, mother_3,
   Message, 29,
   Done,

   10,
   X, CHILDREN,
   Present, children,
   Notat, cottage,
   Message, 23,
   Done,

   8,
   X, AXE,
   Present, axe,
   Message, 26,
   Done,

   8,
   X, GIANT,
   Present, giant,
   Message, 12,
   Done,

   8,
   X, GALLERY,
   At, gallery,
   Message, 35,
   Done,

   8,
   X, KITCHEN,
   At, kitchen,
   Message, 34,
   Done,

   8,
   X, CASTLE,
   Atlt, top_of_beanstalk,
   Message, 15,
   Done,

   6,
   X, CASTLE,
   Message, 33,
   Done,

   8,
   X, COTTAGE,
   At, cottage,
   Message, 36,
   Done,

   6,
   X, ANY,
   Message, 0,
   Done,

   16,
   PLANT, BEAN,
   At, garden,
   Carried, bean,
   Destroy, bean,
   Create, beanstalk,
   Set, FLAG_BEAN,
   Message, 5,
   Done,

   10,
   PLANT, BEAN,
   Notat, garden,
   Zero, FLAG_BEAN,
   Message, 17,
   Done,

   6,
   PLANT, ANY,
   Message, 16,
   Done,

   14,
   OPEN, DOOR,
   At, gallery,
   Absent, open_door,
   Carried, key,
   Message, 2,
   Create, open_door,
   Done,

   12,
   OPEN, DOOR,
   At, huge_room,
   Absent, broken_door,
   Carried, key,
   Message, 8,
   Done,

   10,
   OPEN, DOOR,
   At, gallery,
   Present, open_door,
   Message, 37,
   Done,

   10,
   OPEN, DOOR,
   At, huge_room,
   Notcarr, key,
   Message, 1,
   Done,

   6,
   OPEN, ANY,
   Message, 16,
   Done,

   14,
   BREAK, DOOR,
   At, huge_room,
   Absent, broken_door,
   Carried, axe,
   Message, 7,
   Create, broken_door,
   Done,

   12,
   BREAK, DOOR,
   At, huge_room,
   Absent, broken_door,
   Notcarr, axe,
   Message, 10,
   Done,

   10,
   BREAK, DOOR,
   At, huge_room,
   Present, broken_door,
   Message, 38,
   Done,

   8,
   BREAK, BEAN,
   Present, beanstalk,
   Message, 39,
   Done,

   6,
   BREAK, ANY,
   Message, 16,
   Done,

   10,
   U, BEAN,
   At, garden,
   Present, beanstalk,
   Goto, top_of_beanstalk,
   Desc,

   10,
   GO, DOOR,
   At, gallery,
   Absent, open_door,
   Message, 1,
   Done,

   10,
   GO, DOOR,
   At, gallery,
   Present, open_door,
   Goto, huge_room,
   Desc,

   10,
   GO, DOOR,
   At, huge_room,
   Present, broken_door,
   Goto, green_room,
   Desc,

   6,
   HELP, ANY,
   Message, 9,
   Done,

   17,
   GIVE, EGG,
   At, cottage,
   Present, mother_1,
   Carried, egg,
   Swap, mother_1, mother_2,
   Destroy, egg,
   Message, 18,
   Done,

   15,
   GIVE, EGG,
   At, cottage,
   Present, mother_3,
   Carried, egg,
   Cls,
   Message, 21,
   Message, 22,
   End,

   17,
   GIVE, CHILDREN,
   At, cottage,
   Present, mother_1,
   Carried, children,
   Swap, mother_1, mother_3,
   Destroy, children,
   Message, 20,
   Done,

   15,
   GIVE, CHILDREN,
   At, cottage,
   Present, mother_2,
   Carried, children,
   Cls,
   Message, 19,
   Message, 22,
   End,

   8,
   HIT, GIANT,
   Present, giant,
   Message, 25,
   Done,

   6,
   WAIT, ANY,
   Message, 40,
   Done,

   6,
   TURN, ANY,
   Message, 41,
   Done,

   6,
   TWIST, ANY,
   Message, 42,
   Done,

   6,
   PULL, ANY,
   Message, 43,
   Done,

   6,
   PUSH, ANY,
   Message, 44,
   Done,

   6,
   GET, BEAN,
   Get, bean,
   Ok,

   6,
   GET, KEY,
   Get, key,
   Ok,

   6,
   GET, MOUSE,
   Get, mouse,
   Ok,

   8,
   GET, EGG,
   Notat, huge_room,
   Get, egg,
   Ok,

   10,
   GET, EGG,
   At, huge_room,
   Present, giant,
   Message, 4,
   Done,

   10,
   GET, EGG,
   At, huge_room,
   Absent, giant,
   Get, egg,
   Ok,

   6,
   GET, CHILDREN,
   Get, children,
   Ok,

   6,
   GET, AXE,
   Get, axe,
   Ok,

   4,
   GET, I,
   Inven,

   6,
   DROP, BEAN,
   Drop, bean,
   Ok,

   6,
   DROP, KEY,
   Drop, key,
   Ok,

   8,
   DROP, MOUSE,
   Notat, huge_room,
   Drop, mouse,
   Ok,

   14,
   DROP, MOUSE,
   At, huge_room,
   Present, giant,
   Message, 3,
   Destroy, giant,
   Destroy, mouse,
   Done,

   6,
   DROP, EGG,
   Drop, egg,
   Ok,

   6,
   DROP, CHILDREN,
   Drop, children,
   Ok,

   6,
   DROP, AXE,
   Drop, axe,
   Ok,

   4,
   I, ANY,
   Inven,

   4,
   R, ANY,
   Desc,

   6,
   Q, ANY,
   Quit,
   Turns,
   End,

   4,
   SAVE, ANY,
   Save,

   4,
   LOAD, ANY,
   Load,

   6,
   VERIFY, ANY,
   Message, 45,
   Done,

   0
};
