const char *message[] = { /* 0-254 */
/* 0 */
   "You see nothing special.",
/* 1 */
   "The door is locked.",
/* 2 */
   "OK. You unlock the door with the silver\n"
   "key.",
/* 3 */
   "The mouse frightens the giant who runs\n"
   "away as fast as he can.",
/* 4 */
   "The giant won't let you.",
/* 5 */
   "You plant the bean which grows very\n"
   "fast into a giant beanstalk.",
/* 6 */
   "On the bench you find a silver key.",
/* 7 */
   "You break down the door with the axe.",
/* 8 */
   "The key doesn't fit this lock.",
/* 9 */
   "Sorry!",
/* 10 */
   "How?",
/* 11 */
   "It's a magic bean, perhaps you should\n"
   "plant it.",
/* 12 */
   "He looks mean!",
/* 13 */
   "It looks valuable - it could set you up\n"
   "for life!",
/* 14 */
   "Perhaps you could climb it!",
/* 15 */
   "This is where the mean giant lives!",
/* 16 */
   "You can't do that.",
/* 17 */
   "You can't do that here!",
/* 18 */
   "Mother is pleased with the GOLDEN EGG,\n"
   "now if only you could find THE CHILDREN.",
/* 19 */
   "Mother is so pleased to see THE CHILDREN\n"
   "again and with the GOLDEN EGG you are\n"
   "set up for life and live happily ever\n"
   "after.",
/* 20 */
   "Mother is so pleased to see THE CHILDREN\n"
   "again, now if only you could find the\n"
   "GOLDEN EGG.",
/* 21 */
   "Mother is so pleased that you have been\n"
   "able to find the GOLDEN EGG which will\n"
   "set you up for life and with THE\n"
   "CHILDREN home safe and sound you all\n"
   "live happily ever after.",
/* 22 */
   "\n     C O N G R A T U L A T I O N S\n"
   "\n           This game is over\n",
/* 23 */
   "They look frightened.",
/* 24 */
   "It's just a tiny mouse.",
/* 25 */
   "You must be joking!!! You wouldn't\n"
   "stand a chance!",
/* 26 */
   "Looks sharp!",
/* 27 */
   "It's just a silver key.",
/* 28 */
   "Mother is unhappy because Peter and\n"
   "Mary have been captured by the mean\n"
   "Giant.",
/* 29 */
   "Mother is happy now that the children\n"
   "are back home.",
/* 30 */
   "The garden looks ready for planting.",
/* 31 */
   "The garden has a giant beanstalk growing\n"
   "in it.",
/* 32 */
   "You see nothing special about the shed.",
/* 33 */
   "This is the castle where the giant lives.",
/* 34 */
   "You see nothing special about the\n"
   "kitchen.",
/* 35 */
   "You see nothing special about the\n"
   "gallery.",
/* 36 */
   "You see nothing special about the\n"
   "cottage.",
/* 37 */
   "It's already open.",
/* 38 */
   "It's already broken.",
/* 39 */
   "That wouldn't be wise.",
/* 40 */
   "What for?",
/* 41 */
   "You don't need to turn anything to\n"
   "complete this adventure.",
/* 42 */
   "You don't need to twist anything to\n"
   "complete this adventure.",
/* 43 */
   "You don't need to pull anything to\n"
   "complete this adventure.",
/* 44 */
   "You don't need to push anything to\n"
   "complete this adventure.",
/* 45 */
   "Version 1.2",
/* 46 */
   "You don't have it."
};
