typedef unsigned char byte;
typedef unsigned short word;
typedef int bool;

#define anykey()   fgetc(stdin)
#define cputs(pt)  fputs(pt, stdout)
#define putch(c)   fputc(c, stdout)
#define newline()  fputc('\n', stdout)
#define putsysm(n) fputs(sysmess[n], stdout)
#define putmess(n) fputs(message[n], stdout)
#ifdef __linux__
#define cls()      system("clear")
#else
#define cls()      system("cls")
#ifdef _WIN32
#define sleep(n)   Sleep(n)
#endif
#endif

#define true (1)
#define false (0)
#define NONE       255
#define INPUTMAX   64
#define FLAGMAX    64
#define NOTCREATED 252
#define WORN       253
#define CARRIED    254
#define ITEMMAX (int)(sizeof(Item)/sizeof(*Item))
#define VOCABMAX (int)(sizeof(Vocab)/sizeof(*Vocab))

#define light     (*(flag+0))
#define count     (*(flag+1)) /* only carried objects (not worn) */
#define flag2     (*(flag+2))
#define flag3     (*(flag+3))
#define flag4     (*(flag+4))
#define flag5     (*(flag+5))
#define flag6     (*(flag+6))
#define flag7     (*(flag+7))
#define flag8     (*(flag+8))
#define flag9     (*(flag+9))
#define flag10    (*(flag+10))
/* 11-29 ordinary flags */
/*
#define w3        (*(flag+25))
#define w4        (*(flag+26))
*/
#define score     (*(flag+30))
#define tlsb      (*(flag+31))
#define tmsb      (*(flag+32))
#define w1        (*(flag+33))
#define w2        (*(flag+34))
#define location  (*(flag+35))
#define wid       (*(flag+36))

/*
 * From UnQuill -- done returns:
 * 0 for NOTDONE (fell off end of table)
 * 1 for DONE
 * 2 for DESC
 * 3 for END (end game)
 */
enum { NOTDONE, DONE, DESC, END };

/*
 *
 The Quill & AdventureWriter-list of CondActs
 http://8bitag.com/info/documents/Quill-AdventureWriter-Reference.pdf
 *
 */

enum {
/* conditions */
   At,
   Notat,
   Atgt,
   Atlt,
   Present,
   Absent,
   Worn,
   Notworn,
   Carried,
   Notcarr,
   Chance,
   Zero,
   Notzero,
   Eq,
   Gt,
   Lt,
/* not common */
   Noteq,
   Created,
   Destroyed,
/* actions */
   Inven=0x80u,
   Inv=Inven, /* BBC */
   Desc,
   Quit,
   End,
   Done,
   Ok,
   Anykey,
   Key=Anykey, /* BBC */
   Save,
   Load,
   Turns,
   Score,
   Pause,
   Goto,
   Message,
   Remove,
   Get,
   Wear,
   Drop,
   Destroy,
   Create,
   Swap,
   Set,
   Clear,
   Plus,
   Minus,
   Let,
/* not common */
   Dropall,
   Place,
   Autog,
   Autod,
   Autow,
   Autor,
   Mes,
   Star,
   Sysmess,
   Add,
   Sub,
   Jsr,
   Print,
   Ramsave,
   Ramload,
/* machine-specific displays actions */
   Border,
   Paper,
   Ink,
   Screen,
   Text,
   Cls,
/* machine-specific sound actions */
   BeepEffect,
   Sound,
   Sid,
   Music,
   Volume
};

typedef struct {
   const char *describe;
   const byte *exit;
} Rooms;

typedef struct {
   const char *describe;
   const byte tag;
   const byte initial;
} Items;

typedef struct {
   const byte wn;
   const char *tag;
} Vocabs;

char *filename(void);
bool loadgame(char *str);
bool savegame(char *str);
void diagnostics(void);
void putnum(word num);
void listitems(void);
bool compass(void);
void lookup(const char *str);
bool yesno(int n);
void lineinput(void);
void getinput(void);
void runtable(const byte *table);
void gameloop(void);

int  oid(void);
void oget(byte id);
void odrop(byte id);
void owear(byte id);
void oremove(byte id);
bool olight(void);

/* conditions */
bool at_(void);
bool notat_(void);
bool atgt_(void);
bool atlt_(void);
bool present_(void);
bool absent_(void);
bool worn_(void);
bool notworn_(void);
bool carried_(void);
bool notcarr_(void);
bool chance_(void);
bool zero_(void);
bool notzero_(void);
bool eq_(void);
bool gt_(void);
bool lt_(void);

bool noteq_(void);
bool created_(void);
bool destroyed_(void);

/* actions */
void inven_(void);
void desc_(void);
void quit_(void);
void end_(void);
void done_(void);
void ok_(void);
void anykey_(void);
void save_(void);
void load_(void);
void turns_(void);
void score_(void);
void pause_(void);
void goto_(void);
void message_(void);
void remove_(void);
void get_(void);
void wear_(void);
void drop_(void);
void destroy_(void);
void create_(void);
void swap_(void);
void set_(void);
void clear_(void);
void plus_(void);
void minus_(void);
void let_(void);

void dropall_(void);
void place_(void);
void autog_(void);
void autod_(void);
void autow_(void);
void autor_(void);
void mes_(void);
void star_(void);
void sysmess_(void);
void add_(void);
void sub_(void);
void jsr_(void);
void print_(void);
void ramsave_(void);
void ramload_(void);

void border_(void);
void paper_(void);
void ink_(void);
void screen_(void);
void text_(void);
void cls_(void);

void beep_(void);
void sound_(void);
void sid_(void);
void music_(void);
void volume_(void);

bool (*condition[])(void) = {
   at_,
   notat_,
   atgt_,
   atlt_,
   present_,
   absent_,
   worn_,
   notworn_,
   carried_,
   notcarr_,
   chance_,
   zero_,
   notzero_,
   eq_,
   gt_,
   lt_,
/* not common */
   noteq_,
   created_,
   destroyed_
};

void (*action[])(void) = {
   inven_,
   desc_,
   quit_,
   end_,
   done_,
   ok_,
   anykey_,
   save_,
   load_,
   turns_,
   score_,
   pause_,
   goto_,
   message_,
   remove_,
   get_,
   wear_,
   drop_,
   destroy_,
   create_,
   swap_,
   set_,
   clear_,
   plus_,
   minus_,
   let_,
/* not common */
   dropall_,
   place_,
   autog_,
   autod_,
   autow_,
   autor_,
   mes_,
   star_,
   sysmess_,
   add_,
   sub_,
   jsr_,
   print_,
   ramsave_,
   ramload_,

   border_,
   paper_,
   ink_,
   screen_,
   text_,
   cls_,

   beep_,
   sound_,
   sid_,
   music_,
   volume_
};
