/*
 *
 * cQuill
 * Version 2.1
 * Auraes, 2021-2025
 * Licence Creative Commons BY-SA 4.0 (CC BY-SA 4.0)
 * https://creativecommons.org/licenses/by-sa/4.0/deed.fr
 *
 * The Quill is copyright Gilsoft and its creators 1983-2023.
 * UnQuill is copyright 1996-2002, 2011, 2012, 2015, 2023 by John Elliott.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#ifdef __linux__
#include <unistd.h>
#elif _WIN32
#include <windows.h>
#else /* MSDOS */
#include <dos.h>
#endif
#include "cquill.h"
#include "vocabs.h"
#include "rooms.h"
#include "items.h"
#include "sysmess.h"
#include "messages.h"
#include "events.h"

FILE *fp;
const byte *ip;
byte flag[FLAGMAX+ITEMMAX];
byte ram[1+FLAGMAX+ITEMMAX];
byte *at = flag+FLAGMAX;
char buffer[INPUTMAX+1];
int done;
bool diagnostic;

#include "condacts.c"

char *filename(void)
{
   putsysm(33);
   lineinput();
   if (*buffer) {
      *(buffer+8) = '\0';
      return buffer;
   }
   return NULL;
}

bool loadgame(char *s)
{
   fp = fopen(s, "rb");
   if (!fp)
      return false;
   fread(flag, 1, (sizeof(byte)*(FLAGMAX+ITEMMAX)), fp);
   fclose(fp);
   return true;
}

bool savegame(char *s)
{
   fp = fopen(s, "wb");
   if (!fp)
      return false;
   fwrite(flag, 1, (sizeof(byte)*(FLAGMAX+ITEMMAX)), fp);
   fclose(fp);
   return true;
}

/* "Do you require diagnostics?" */
void diagnostics(void)
{
   int i;

   newline();
   for (i=1; i<=36; i++) {
      printf("%3d ", flag[i-1]);
      if (!(i % 10))
         newline();
   }
   newline();
}

void putnum(word n)
{
   char *p; /* WARNING buffer */

   p = buffer + 5;
   *p = '\0';
   do {
      --p;
      *p = n % 10 + '0';
   } while (n /= 10);
   cputs(p);
}

void listitems(void)
{
   int i;
   bool flg;

   flg = true;
   for (i=0; i<ITEMMAX; i++) {
      if (at[i] == location) {
         if (flg) {
            flg = false;
            putsysm(1);
         }
         cputs(Item[i].describe);
         newline();
      }
   }
}

bool compass(void)
{
   const byte *p;

   p = Room[location].exit;
   if (p) {
      while (*p) {
         if (*p == w1) {
            location = *(p+1);
            return true;
         }
         p += 2;
      }
   }
   return false;
}

void lookup(const char *s)
{
   int i, n;

   n = 0;
   for (i=0; i<VOCABMAX; i++) {
      if (n < Vocab[i].wn) {
         n = Vocab[i].wn;
         wid = i;
      }
      if (strncmp(s, Vocab[i].tag, TRUNCATE) == 0)
         break;
   }
}

bool yesno(int n)
{
   lineinput();
   if (*buffer == tolower(*sysmess[n]))
      return true;
   return false;
}

void lineinput(void)
{
   char *p;
   int c;

   putch('>');
   p = buffer;
   if (fgets(p, INPUTMAX+1, stdin)) {
      while (*p != '\0') {
         if (*p == '\n') {
            *p = '\0';
            return;
         }
         *p = tolower(*p);
         ++p;
      }
      do {
         c = fgetc(stdin);
      } while (c != '\n' && c != EOF);
   }
}

void getinput(void)
{
   char *p;

   lineinput();
   w1 = 0xffu;
   p = strtok(buffer, " ");
   while (p) {
      lookup(p);
      if (wid < (VOCABMAX-1)) {
         if (w1 == 0xffu) {
            w1 = Vocab[wid].wn;
            w2 = 0xffu;
         }
         else {
            w2 = Vocab[wid].wn;
            break;
         }
      }
      p = strtok(NULL, " ");
   }
}

void runtable(const byte *p)
{
   const byte *ips;
   int actdone;

   done = actdone = NOTDONE;
   ip = ips = p;
   while (*ip) {
      ips += *ip;
      ++ip;
      if ((p == StatusTable)
         || ((*ip == w1 && (*(ip+1) == w2 || *(ip+1) == ANY))
         || *ip == ANY)) {
         ip += 2;
         while (ip < ips) {
            if ((*ip) & 0x80u) {
               actdone = DONE;
               (action[(*ip) & 0x7fu])();
               if (done)
                  return;
            }
            else if (!(condition[*ip])())
               break;
            ++ip;
         }
      }
      ip = ips;
   }
   done = actdone;
}

void gameloop(void)
{
   int i;

   i = FLAGMAX;
   while (i)
      flag[--i] = 0;
   *ram = 0;
   i = ITEMMAX;
   while (i) {
      --i;
      at[i] = Item[i].initial;
      if (at[i] == CARRIED)
         ++count;
   }

   redescribe:
   cls_();
   if (flag2)
      --flag2;
   if (light && flag3)
      --flag3;
   if (light && !olight() && flag4)
      --flag4;
   if (light == 0 || olight()) {
      cputs(Room[location].describe);
      newline();
      listitems();
   }
   else
      putsysm(0);

   while (true) {
      runtable(StatusTable);
      if (done == DESC)
         goto redescribe;
      if (done == END)
         break;

      if (flag5)
         --flag5;
      if (flag6)
         --flag6;
      if (flag7)
         --flag7;
      if (flag8)
         --flag8;
      if (light && flag9)
         --flag9;
      if (light && !olight() && flag10)
         --flag10;

       ++tlsb;
      if (tlsb == 0)
         ++tmsb;
      if (diagnostic)
         diagnostics();
      putsysm((rand() % 4) + 2);
      getinput();

      if (w1 < 0xffu) {
         if (compass())
            goto redescribe;
         runtable(EventTable);
         if (done == DESC)
            goto redescribe;
         if (done == END)
            break;
         if (done == NOTDONE) {
            if (w1 < 0x0du)
               putsysm(7);
            else
               putsysm(8);
         }
      }
      else
         putsysm(6);
   }
}

int main(int argc, char *argv[])
{
   if (argc > 1 && strcmp(argv[1], "-d") == 0)
      diagnostic = true;
   srand((unsigned int) time(NULL));
   do {
      gameloop();
   } while (!yesno(31));
   putsysm(14);

   return EXIT_SUCCESS;
}
