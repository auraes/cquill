**cQuill**  
Version 2.1  
Auraes, 2021-2025.  
Licence Creative Commons BY-SA 4.0 (CC BY-SA 4.0)  
https://creativecommons.org/licenses/by-sa/4.0/deed.fr

cQuill permet d'adapter des jeux d’aventure textuels créés avec le logiciel  
The Quill/Adventure Writer pour le langage C (GCC Linux). Les jeux sont  
préalablement décompilés avec le logiciel UnQuill.

Pour plus d’informations sur le jeu d’instructions, consultez les  
manuels de la version Zx Spectrum et Commodore 64 de The Quill (Gilsoft).  
Il est aussi possible de créer directement son propre jeu.

L'option `-d`, en ligne de commande, permet de lancer le mode diagnostique.

**Compilation**  
GCC :  
`gcc -Wall -W  -std=c99 src/cquill.c -Igames/lotg -olotg`

Windows – MinGw :  
`gcc -Wall -W  -std=c99 src/cquill.c -Igames/lotg -olotg`

DOS – Borland Turbo C++ 3.00 :  
`tcc -w -a -K -ms -d -Igames/lotg -Isrc -elotg src/cquill.c`

**Logiciels et documentations externes**  
http://8-bit.info/the-gilsoft-adventure-systems/  
https://www.seasip.info/Unix/UnQuill/index.html  
http://8bitag.com/info/  
http://8bitag.com/info/documents/Quill-AdventureWriter-Reference.pdf  
https://s3-eu-west-2.amazonaws.com/quill-adventure-guides/main.html  
https://www.ifarchive.org/indexes/if-archiveXprogrammingXquill.html  
https://www.filfre.net/2013/07/the-quill/  
http://www.atarimania.com/game-atari-400-800-xl-xe-adventurewriter_20331.html

**Crédits**  
The Quill is copyright Gilsoft and its creators 1983-2024.  
UnQuill is copyright 1996-2002, 2011, 2012, 2015, 2020 by John Elliott.

Land of the Giants is copyright Dorothy Millard 1989.  
http://members.optusnet.com.au/dorothyirene1/adv.html  
https://www.solutionarchive.com/game/id%2C5061/Land+of+the+Giants.html